import {Character} from "./Character";
import {GoldenTrident} from "./GoldenTrident";

const prettyColors: any = {
    BLACK: 0x111111,
    BLUE: 0x0000FF,
    CYAN: 0x00FFFF,
    GREEN: 0x00FF00,
    MAGENTA: 0xFF00FF,
    ORANGE: 0xFF9900,
    RED: 0xFF0000,
    YELLOW: 0xFFFF00,
};

const prettyColorsArray: number[] =
    Object.keys(prettyColors).map((key) => prettyColors[key]);

// TODO: Minimpa implementation need to be reviewed
export class Minimap {
    private _game: Phaser.Game;
    private _player: Character;
    private _teams = new Map<number, Phaser.Group>();
    private _goldenTrident: GoldenTrident;
    private _factor: number;

    private _graphBackground: Phaser.Graphics;
    private _graphPoints: Phaser.Graphics;

    constructor(game: Phaser.Game,
                player: Character,
                teams: Map<number, Phaser.Group>,
                goldenTrident: GoldenTrident,
                factor: number) {
        this._game = game;
        this._player = player;
        this._teams = teams;
        this._goldenTrident = goldenTrident;
        this._factor = factor;

        this._graphPoints = this._game.add.graphics(10, 10);
        this._graphBackground = this._game.add.graphics(10, 10);
        this._graphPoints.fixedToCamera = true;
        this._graphBackground.fixedToCamera = true;

        this._graphBackground.beginFill(0xFFFFFF, 0.5);
        this._graphBackground.drawRect(
            0,
            0,
            this._game.world.width / this._factor,
            this._game.world.height / this._factor);
    }

    set player(value: Character) {
        this._player = value;
    }

    public update() {
        this._graphPoints.clear();
        this._game.world.bringToTop(this._graphPoints);

        if (this._goldenTrident) {
            if (this._goldenTrident.isCarried()) {
                this._drawCharacter(this._goldenTrident.player, 0xFFFF00);
            } else {
                this._drawCharacter(this._goldenTrident.sprite, 0xFFFF00);
            }
        }

        for (const [teamId, group] of this._teams.entries()) {
            group.forEach((sprite: Phaser.Sprite) => {
                    this._drawCharacter(sprite, prettyColorsArray[teamId]);
                },
                this,
            );
        }

        if (this._player !== null) {
            // scopeRadius
            this._graphPoints.beginFill(0x00FFFF, 0.2);
            this._graphPoints.drawCircle(
                this._player.position.x / this._factor,
                this._player.position.y / this._factor,
                2 * this._player.characterClass.scopeRadius / this._factor);

            // player position
            this._drawCharacter(this._player, 0x0000FF);
        }
    }

    private _drawCharacter(sprite: Phaser.Sprite, color: number) {
        let opacity = 0.7;
        if (!sprite.alive) {
            opacity = 0.2;
        }

        this._graphPoints.beginFill(color, opacity);
        this._graphPoints.drawCircle(
            sprite.position.x / this._factor,
            sprite.position.y / this._factor, 5);
        this._graphPoints.endFill();
    }
}
