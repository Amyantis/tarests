import {AppearanceSkin, IFrameSize} from "./AppearanceSkin";

// tslint:disable:max-classes-per-file

export abstract class Ammo extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class AmmoArrow extends Ammo {
    protected static _file = "weapons/left hand/either/arrow.png";
    // TODO: fix this (BodySkeleton has to be defined before...)
    // protected static _prohibited = BodySkeleton;

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Armor extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class ArmorChestGold extends Armor {
    protected static _fileFemale = "torso/gold/chest_female.png";
    protected static _fileMale = "torso/gold/chest_male.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class ArmorChestLeather extends Armor {
    protected static _fileFemale = "torso/leather/chest_female.png";
    protected static _fileMale = "torso/leather/chest_male.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class ArmorChestPlate extends Armor {
    protected static _fileFemale = "torso/plate/chest_female.png";
    protected static _fileMale = "torso/plate/chest_male.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Arms extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class ArmsGold extends Arms {
    protected static _fileFemale = "torso/gold/arms_female.png";
    protected static _fileMale = "torso/gold/arms_male.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class ArmsPlate extends Arms {
    protected static _fileFemale = "torso/plate/arms_female.png";
    protected static _fileMale = "torso/plate/arms_male.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Belts extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class BeltLeather extends Belts {
    protected static _fileFemale = "belt/leather/female/leather_female.png";
    protected static _fileMale = "belt/leather/male/leather_male.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Body extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class BodyLight extends Body {
    protected static _fileFemale = "body/female/light.png";
    protected static _fileMale = "body/male/light.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class BodyDark extends Body {
    protected static _fileFemale = "body/female/dark.png";
    protected static _fileMale = "body/male/dark.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class BodyOrc extends Body {
    protected static _fileFemale = "body/female/orc.png";
    protected static _fileMale = "body/male/orc.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class BodySkeleton extends Body {

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Bracelet extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Bracers extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Buckles extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Cape extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class CapeBrown extends Cape {
    protected static _fileFemale =
        "torso/back/cape/normal/female/cape_brown.png";
    protected static _fileMale = CapeBrown._fileFemale;

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class CapeGray extends Cape {
    protected static _fileFemale =
        "torso/back/cape/normal/female/cape_gray.png";
    protected static _fileMale = CapeGray._fileFemale;

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class CapeYellow extends Cape {
    protected static _fileFemale =
        "torso/back/cape/normal/female/cape_yellow.png";
    protected static _fileMale = CapeYellow._fileFemale;

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Clothes extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Ears extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Eyes extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Gloves extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class GlovesGold extends Gloves {
    protected static _fileFemale =
        "hands/gloves/female/golden_gloves_female.png";
    protected static _fileMale =
        "hands/gloves/male/golden_gloves_male.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class GlovesMetal extends Gloves {
    protected static _fileFemale =
        "hands/gloves/female/metal_gloves_female.png";
    protected static _fileMale =
        "hands/gloves/male/metal_gloves_male.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Greaves extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class GreavesMetal extends Greaves {
    protected static _fileFemale =
        "legs/armor/female/metal_pants_female.png";
    protected static _fileMale =
        "legs/armor/male/metal_pants_male.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class GreavesGold extends Greaves {
    protected static _fileFemale =
        "legs/armor/female/golden_greaves_female.png";
    protected static _fileMale =
        "legs/armor/male/golden_greaves_male.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Hair extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class HairBlonde extends Hair {
    protected static _fileFemale =
        "hair/female/princess/blonde.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class HairBrown extends Hair {
    protected static _fileFemale =
        "hair/female/princess/brown.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class HairGray extends Hair {
    protected static _fileFemale =
        "hair/female/princess/gray.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Hat extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class HatChain extends Hat {
    protected static _fileFemale =
        "head/helms/female/chainhat_female.png";
    protected static _fileMale =
        "head/helms/male/chainhat_male.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class HatGold extends Hat {
    protected static _fileFemale =
        "head/helms/female/golden_helm_female.png";
    protected static _fileMale =
        "head/helms/male/golden_helm_male.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class HatMetal extends Hat {
    protected static _fileFemale =
        "head/helms/female/metal_helm_female.png";
    protected static _fileMale =
        "head/helms/male/metal_helm_male.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class HatHoodCloth extends Hat {
    protected static _fileFemale =
        "head/hoods/female/cloth_hood_female.png";
    protected static _fileMale =
        "head/hoods/male/cloth_hood_male.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Jacket extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Mail extends AppearanceSkin {
    protected static _fileFemale = "torso/chain/mail_female.png";
    protected static _fileMale = "torso/chain/mail_male.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Necklaces extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Nose extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Quiver extends AppearanceSkin {
    protected static _file = "behind_body/equipment/quiver.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class Shield extends AppearanceSkin {
    protected static _fileHat =
        "weapons/left hand/male/shield_male_cutoutforbody.png";
    protected static _fileNoHat =
        "weapons/left hand/male/shield_male_cutoutforhat.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Shoes extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class ShoesGold extends Shoes {
    protected static _fileFemale =
        "feet/armor/female/golden_boots_female.png";
    protected static _fileMale =
        "feet/armor/male/golden_boots_male.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class ShoesMetal extends Shoes {
    protected static _fileFemale =
        "feet/armor/female/metal_boots_female.png";
    protected static _fileMale =
        "feet/armor/male/metal_boots_male.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Shoulders extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Spikes extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Tie extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export abstract class Weapon extends AppearanceSkin {
    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class WeaponSpear extends Weapon {
    protected static _fileFemale =
        "weapons/right hand/female/spear_female.png";
    protected static _fileMale =
        "weapons/right hand/male/spear_male.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class WeaponBow extends Weapon {
    protected static _file =
        "weapons/right hand/either/bow.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }
}
export class WeaponTrident extends Weapon {
    protected static _frameSize: IFrameSize = {
        height: 192,
        width: 192,
    };
    protected static _file =
        "weapons/oversize/two hand/either/trident_extended.png";

    constructor(game: Phaser.Game, file: string) {
        super(game, file);
    }

    public update() {
        if (typeof this._holder !== "undefined") {
            const nbOfFramePerLine = 13;
            const nbOfUsedFramePerLine = 9;
            const nbOfUnusedFramePerLine =
                nbOfFramePerLine - nbOfUsedFramePerLine;
            const frameOffset = 52;

            this.frame = this._computeFrame(
                nbOfFramePerLine, nbOfUnusedFramePerLine, frameOffset);
        }
    }

    private _computeFrame(nbFramePerLine: number,
                          nbUnusedFramePerLine: number,
                          frameOffset: number,
                          holderCurrentFrame?: number) {
        holderCurrentFrame = holderCurrentFrame || <number> this._holder.frame;
        holderCurrentFrame -= frameOffset;
        return holderCurrentFrame -
            Math.floor(holderCurrentFrame / nbFramePerLine) *
            nbUnusedFramePerLine;
    }
}

export const appearanceSkins = new Set([
    AmmoArrow,
    ArmorChestGold,
    ArmorChestLeather,
    ArmorChestPlate,
    ArmsGold,
    ArmsPlate,
    BeltLeather,
    BodyLight,
    BodyDark,
    BodyOrc,
    BodySkeleton,
    CapeBrown,
    CapeGray,
    CapeYellow,
    GlovesGold,
    GlovesMetal,
    GreavesGold,
    GreavesMetal,
    HairBlonde,
    HairBrown,
    HairGray,
    HatChain,
    HatGold,
    HatMetal,
    HatHoodCloth,
    Mail,
    Quiver,
    Shield,
    ShoesGold,
    ShoesMetal,
    WeaponSpear,
    WeaponBow,
    WeaponTrident,
]);
