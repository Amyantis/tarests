import {Sex} from "../../protocol/sharedInterfaces";
import {Appearance} from "./Appearance";
import {ModularCharacterSprite} from "./ModularCharacterSprite";

export interface IFrameSize {
    height: number;
    width: number;
}

export interface IRessource {
    filesPaths: string[];
    frameSize: IFrameSize;
}

export class AppearanceSkin extends Phaser.Sprite {
    public static correspondingFile(characterAppearance: Appearance) {
        if (this._fileHat !== null &&
            characterAppearance.classAppearance.hat !== null) {
            return this._fileHat;
        }
        if (this._fileNoHat !== null &&
            characterAppearance.classAppearance.hat === null) {
            return this._fileNoHat;
        }
        if (this._fileMale !== null &&
            characterAppearance.sex === Sex.Male) {
            return this._fileMale;
        }
        if (this._fileFemale !== null &&
            characterAppearance.sex === Sex.Female) {
            return this._fileFemale;
        }
        if (this._file !== null) {
            return this._file;
        }
        throw Error("No file available");
    }

    public static attach(character: ModularCharacterSprite) {
        const file = this.correspondingFile(character.appearance);
        const skinConstructor =
            <typeof AppearanceSkin> this.prototype.constructor;
        const skin = new skinConstructor(character.game, file);
        skin._holder = character;
        skin._holder.addChild(skin);
        return skin;
    }

    protected static _frameSize: IFrameSize = {
        height: 64,
        width: 64,
    };
    protected static _file: string = null;
    protected static _fileFemale: string = null;
    protected static _fileMale: string = null;
    protected static _fileHat: string = null;
    protected static _fileNoHat: string = null;
    protected static _prohibited: AppearanceSkin = null;

    protected static _requiredSkin: AppearanceSkin = null;

    static get frameSize(): IFrameSize {
        return this._frameSize;
    }

    static get file(): string {
        return this._file;
    }

    static get fileFemale(): string {
        return this._fileFemale;
    }

    static get fileMale(): string {
        return this._fileMale;
    }

    static get fileHat(): string {
        return this._fileHat;
    }

    static get fileNoHat(): string {
        return this._fileNoHat;
    }

    static get files(): Set<string> {
        const files = [
            this.file,
            this.fileFemale,
            this.fileMale,
            this.fileHat,
            this.fileNoHat,
        ].filter((file) => file !== null);
        return new Set(files);
    }

    static get neededRessources(): IRessource {
        const files = new Array<string>();
        if (this._fileHat !== null) {
            files.push(this._fileHat);
        }
        if (this._fileNoHat !== null) {
            files.push(this._fileNoHat);
        }
        if (this._fileMale !== null) {
            files.push(this._fileMale);
        }
        if (this._fileFemale !== null) {
            files.push(this._fileFemale);
        }
        if (this._file !== null) {
            files.push(this._file);
        }
        return {
            filesPaths: files,
            frameSize: this.frameSize,
        };
    }

    protected _holder: Phaser.Sprite;

    constructor(game: Phaser.Game, file: string) {
        super(game, 0, 0, file, 1);
        this._checkUsedFile(file);
        this.anchor.setTo(0.5, 0.5);
        game.add.existing(this);
    }

    set holder(value: Phaser.Sprite) {
        this._holder = value;
    }

    public update() {
        this.frame = this._holder.frame;
    }

    private _checkUsedFile(file: string) {
        const appearanceSkin = <typeof AppearanceSkin> this.constructor;
        if (!appearanceSkin.files.has(file)) {
            throw Error(`Unknown file ${file} for this skin.`);
        }
    }
}
