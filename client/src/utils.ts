export function computeRoundedAverage(list: number[]) {
    if (list.length === 0) {
        return 0.0;
    }

    let average = 0.0;
    for (const element of list) {
        average += element;
    }
    return Math.round(average / list.length);
}

export function debugVars(vars: Array<string |
                              number |
                              Array<string | number>>,
                          x: number, y: number,
                          game: Phaser.Game, color: string = "white") {
    for (const info of vars) {
        if (Array.isArray(info)) {
            game.debug.text(info.join("\t"), x, y, color);
        } else {
            game.debug.text(info.toString(), 32, y, color);
        }
        y += 28;
    }
}
