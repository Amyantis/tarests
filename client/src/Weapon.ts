import {Character} from "./Character";
import {Game} from "./Game";
import {Skill} from "./Skill";

interface ITargetReached {
    deltaTime: number;
    target: Character;
}

export class Weapon {
    private _weapon: Phaser.Weapon;
    private _character: Character;
    private _lastFire: number = 0;
    private _targetReachedDates = new Array<ITargetReached>();

    constructor(character: Character) {
        const game = <Game> character.game;
        this._weapon =
            game.add.weapon(40, "img/ammo/rgblaser.png");
        this._character = character;

        this._weapon.trackSprite(this._character);
        this._weapon.setBulletFrames(0, 80, true);
        this._weapon.fireRate = 10;
        this._weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this._weapon.bulletSpeed = 500;
    }

    public updateForElaspedTime(elaspedMS: number): void {
        this._lastFire += elaspedMS;
        this._weapon.update();

        for (const v of this._targetReachedDates) {
            v.deltaTime -= elaspedMS;
            if (v.deltaTime <= 0) {
                this._handleHit(this._targetReachedDates.shift().target);
            }
        }

        if (this._lastFire > this._character.currentAutoAttackSpeed * 1e3 &&
            typeof this._character.target !== "undefined" &&
            this._character.target !== null &&
            this._character.target.teamId !== this._character.teamId &&
            this._character.target.alive &&
            Skill.isPlayerCloseEnough(this._character)) {
            this._lastFire = 0;

            const bullet =
                this._weapon.fireAtSprite(this._character.target);

            this._playShooterAnimation();

            const distanceBtwnPlayerAndTarget =
                Skill.distanceBtwnPlayerAndTarget(this._character);
            const timeToReachTargetMS =
                distanceBtwnPlayerAndTarget / this._weapon.bulletSpeed *
                this._character.currentAutoAttackSpeed * 1e3;
            // TODO: find a workaround to avoid using setTimeout
            setTimeout(() => {
                bullet.kill();
            }, timeToReachTargetMS);

            this._targetReachedDates.push({
                deltaTime: timeToReachTargetMS,
                target: this._character.target,
            });
        }

        // Don't use following
        // const game = <Game> this._character.game;
        // for (const player of game.characterList.values()) {
        //     game.physics.arcade.collide(
        //         this._weapon.bullets, player,
        //         (character: Character, bullet: Phaser.Bullet) => {
        //             bullet.kill();
        //         });
        // }
    }

    private _playShooterAnimation() {
        const x = this._character.x - this._character.target.x;
        const y = this._character.y - this._character.target.y;
        let thetaRadians = Math.atan2(y, x);
        let animation: string;
        if (thetaRadians > Math.PI / 4 && thetaRadians <= 3 * Math.PI / 4) {
            animation = "shoot_up";
        }
        if ((thetaRadians > 3 * Math.PI / 4 && thetaRadians <= Math.PI) ||
            (thetaRadians <= -3 * Math.PI / 4 && thetaRadians >= -Math.PI)) {
            animation = "shoot_right";
        }
        if (thetaRadians > -3 * Math.PI / 4 && thetaRadians <= -Math.PI / 4) {
            animation = "shoot_down";
        }
        if ((thetaRadians > -Math.PI / 4 && thetaRadians < 0) ||
            (thetaRadians <= Math.PI / 4 && thetaRadians > 0)) {
            animation = "shoot_left";
        }
        // TODO: shoot animation doesn't play correctly (only one frame played?)
        this._character.play(animation);
    }

    private _handleHit(target: Character) {
        target.damage(this._character.currentAutoAttackDamage);
    }
}
