import {Character} from "./Character";
import {PointsManager} from "./PointsManager";

export class UserInterface {
    public static setProgressBar(barElement: JQuery,
                                 progress: number,
                                 maxProgress: number) {
        const percentage =
            Math.max(
                Math.min(
                    Math.floor(progress / maxProgress * 100),
                    100),
                0);
        barElement.width(`${percentage}%`);
        const text =
            `${Math.min(Math.round(progress), maxProgress)}/${maxProgress}`;
        barElement.text(text);
    }

    private _player: Character;
    private _allies: Phaser.Group;

    private _sendSkillUseEvent: any;

    private _skillInfo: JQuery;
    private _skillButtons: JQuery;

    private _playerNameDiv: JQuery;
    private _playerAutoAttackDamageDiv: JQuery;
    private _playerAutoAttackSpeedDiv: JQuery;
    private _playerShieldDiv: JQuery;
    private _playerSpeedDiv: JQuery;
    private _playerLifeDiv: JQuery;
    private _playerEnergyDiv: JQuery;
    private _alliesInfosDiv: JQuery;
    private _targetNameDiv: JQuery;
    private _targetLifeDiv: JQuery;

    private _pointsManager: PointsManager;

    constructor(player: Character,
                allies: Phaser.Group,
                sendSkillUseEventMethod: any,
                pointsManager: PointsManager) {
        this._player = player;
        this._allies = allies;
        this._sendSkillUseEvent = sendSkillUseEventMethod;
        this._pointsManager = pointsManager;

        this._skillInfo = $(".skill_info");
        this._skillButtons = $(".skill_bar button");

        const playerDiv = $(".player");
        this._playerNameDiv = playerDiv.find(".player_name");
        this._playerAutoAttackDamageDiv = playerDiv.find(
            ".player_autoAttackDamage");
        this._playerAutoAttackSpeedDiv = playerDiv.find(
            ".player_autoAttackSpeed");
        this._playerShieldDiv = playerDiv.find(".player_shield");
        this._playerSpeedDiv = playerDiv.find(".player_speed");
        this._playerLifeDiv = playerDiv.find(".player_life");
        this._playerEnergyDiv = playerDiv.find(".player_energy");

        this._alliesInfosDiv = $(".allies .ally_info");

        const targetDiv = $(".target");
        this._targetNameDiv = targetDiv.find(".target_name");
        this._targetLifeDiv = targetDiv.find(".target_life");

        $(".scores").hide();
        $("body").keyup((e: JQueryEventObject) => {
            let code = e.keyCode || e.which;
            if (code === 16 || code === 17) {
                $(".scores").hide();
            }
        });
        $("body").keydown((e: JQueryEventObject) => {
            let code = e.keyCode || e.which;
            if (code === 16 || code === 17) {
                $(".scores").show();
            }
        });

        this.updateSkillBar();
        this.update();
    }

    public updateSkillBar() {
        const skills = this._player.characterClass.skills;

        this._skillInfo.hide();
        this._skillButtons.hide();

        // TODO: move this away
        function sendSkillUseEvent(i: number) {
            this._sendSkillUseEvent(i);
        }

        function showSkillInfo(i: number) {
            this._skillInfo.show();
            const skill = skills[i];
            const info =
                `<table><tr><td>
                Temps d'incantation: <b>${skill.castTime / 1000}s</b></td><td>
                Temps de recharge: <b>${skill.coolDownTime / 1000}s</b></td>
                </tr><tr><td>
                Coût en énergie: <b>${skill.energyCost}</b></td><td>
                Coût en point de vie: <b>${skill.lifeCost}</b></td></tr>`;

            this._skillInfo.find(".skill_name").text(skill.name);
            this._skillInfo.find(".skill_desc").text(skill.description);
            this._skillInfo.find(".skill_details").html(info);
        }

        function hideSkillInfo() {
            this._skillInfo.hide();
        }

        let i = 0;
        for (const skill of skills) {
            const skillButton = $(this._skillButtons[i]);
            skillButton.show();

            skillButton.css("background-image", "url(" + skill.fileIcon + ")");

            skillButton.click(sendSkillUseEvent.bind(this, i + 1));
            skillButton.mouseover(showSkillInfo.bind(this, i));
            skillButton.mouseout(hideSkillInfo.bind(this));

            i++;
        }
    }

    public update() {
        this.updateSkillProgressBar();

        this._playerNameDiv.text(this._player.name);
        this._playerAutoAttackDamageDiv.text(
            this._player.currentAutoAttackDamage);
        this._playerAutoAttackSpeedDiv.text(
            this._player.currentAutoAttackSpeed + " s");
        this._playerSpeedDiv.text(this._player.currentSpeed);
        this._playerShieldDiv.text(this._player.currentShield);

        UserInterface.setProgressBar(
            this._playerLifeDiv,
            this._player.health,
            this._player.maxHealth);

        UserInterface.setProgressBar(
            this._playerEnergyDiv,
            this._player.currentEnergy,
            this._player.characterClass.maxEnergy);

        this._targetNameDiv.text(this._player.target.name);

        UserInterface.setProgressBar(
            this._targetLifeDiv,
            this._player.target.health,
            this._player.target.maxHealth);

        let i = 0;
        this._allies.forEach((sprite: Phaser.Sprite) => {
                if (sprite === this._player) {
                    return;
                }
                const allyInfo = $(this._alliesInfosDiv[i]);
                allyInfo.show();
                const allyNameElement =
                    allyInfo.find(".ally_name");
                allyNameElement.text(sprite.name);

                const progressBar =
                    allyInfo.find(".progress-bar");
                progressBar.show();

                UserInterface.setProgressBar(
                    progressBar,
                    sprite.health,
                    sprite.maxHealth);
                i++;
            },
            this,
        );
        for (; i < this._alliesInfosDiv.length; i++) {
            $(this._alliesInfosDiv[i]).hide();
        }

        $(".scores tr:not(:first)").hide();
        const teamNames = ["Human", "Light", "Orc", "Metal"];
        i = 0;
        for (const [teamId, points] of this._pointsManager.points.entries()) {
            i++;
            const trAssociated = $(`.scores tr:eq(${i})`);
            trAssociated.show();
            trAssociated.find("td:eq(0)").text(teamNames[teamId]);
            trAssociated.find("td:eq(1)").text(Math.round(points));
        }
    }

    private updateSkillProgressBar() {
        const skills = this._player.characterClass.skills;

        let i = 0;
        for (const skill of skills) {
            UserInterface.setProgressBar(
                $(this._skillButtons[i]).find(".skill_cooldown"),
                skill.elaspedSinceLastUsed,
                skill.coolDownTime);
            i++;
        }
    }
}
