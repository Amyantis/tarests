import {Character} from "./Character";
import {Game} from "./Game";

// Singleton
export class PointsManager {
    private _game: Game;

    private _teams: Map<number, Phaser.Group>;
    private _points = new Map<number, number>();

    private _pointsToWin: number;

    constructor(game: Game,
                teams: Map<number, Phaser.Group>,
                pointsToWin: number = 100) {
        this._game = game;
        this._teams = teams;
        for (const teamId of this._teams.keys()) {
            this._points.set(teamId, 0);
        }
        this._pointsToWin = pointsToWin;
    }

    get points(): Map<number, number> {
        return this._points;
    }

    public getPoints(teamId: number) {
        this.assertThatTeamExists(teamId);
        return this._points.get(teamId);
    }

    public addPointsAgainst(teamId: number,
                            pointsToAdd: number) {
        for (const team of this._points.keys()) {
            if (team !== teamId) {
                this.addPoints(team, pointsToAdd);
            }
        }
    }

    public addPoints(teamId: number, pointsToAdd: number) {
        if (pointsToAdd <= 0) {
            console.log("Removing", pointsToAdd, "from", teamId);
        }
        const points = this.getPoints(teamId);
        const totalPoints = points + pointsToAdd;
        this._points.set(teamId, totalPoints);

        if (totalPoints > this._pointsToWin) {
            this.handleVictory(teamId);
        }
    }

    private handleVictory(winningTeamId: number) {
        $(".starting_screen").hide();
        $(".game_screen").hide();
        $(".introduction_screen").hide();
        $(".ending_screen").show();

        const endingScreenRows = $(".ending_screen tr:not(:first)");
        endingScreenRows.hide();

        const teamNames = ["Human", "Light", "Orc", "Metal"];
        let i = 0;
        for (const [team, points] of this._points.entries()) {
            i++;
            const trAssociated = $(`.ending_screen tr:eq(${i})`);
            trAssociated.show();
            trAssociated.find("td:eq(0)").text(teamNames[team]);
            trAssociated.find("td:eq(2)").text(Math.round(points));

            const listItems = trAssociated.find("td:eq(1) li");
            listItems.hide();

            const playersGroup = this._teams.get(team);
            let j = -1;
            playersGroup.forEach((player: Character) => {
                    j++;
                    const li = $(listItems[j]);
                    li.text(player.name);
                    li.show();
                },
                this,
            );
        }

        // best part starts here: DESTROY EVERYTHING
        setTimeout(() => {
            this._game.socket.close();
            this._game.paused = true;
            try {
                this._game.destroy();
            } finally {
                console.warn("Everything destroyed");
            }
        }, 300);
    }

    private assertThatTeamExists(teamId: number) {
        if (!this._points.has(teamId)) {
            throw Error(`Team ${teamId} doesn't exists.`);
        }
    }
}
