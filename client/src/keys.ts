export const numberToPhaserKey = new Map<number, number>([
    [0, Phaser.Keyboard.ZERO],
    [1, Phaser.Keyboard.ONE],
    [2, Phaser.Keyboard.TWO],
    [3, Phaser.Keyboard.THREE],
    [4, Phaser.Keyboard.FOUR],
    [5, Phaser.Keyboard.FIVE],
    [6, Phaser.Keyboard.SIX],
    [7, Phaser.Keyboard.SEVEN],
    [8, Phaser.Keyboard.EIGHT],
    [9, Phaser.Keyboard.NINE],
]);
export const phaserKeyToSkillIndex = new Map<number, number>([
    [Phaser.Keyboard.ZERO, 0],
    [Phaser.Keyboard.ONE, 1],
    [Phaser.Keyboard.TWO, 2],
    [Phaser.Keyboard.THREE, 3],
    [Phaser.Keyboard.FOUR, 4],
    [Phaser.Keyboard.FIVE, 5],
    [Phaser.Keyboard.SIX, 6],
    [Phaser.Keyboard.SEVEN, 7],
    [Phaser.Keyboard.EIGHT, 8],
    [Phaser.Keyboard.NINE, 9],
]);

export const listenedKeys = new Set([
    Phaser.Keyboard.UP,
    Phaser.Keyboard.DOWN,
    Phaser.Keyboard.LEFT,
    Phaser.Keyboard.RIGHT,
    Phaser.Keyboard.Z,
    Phaser.Keyboard.S,
    Phaser.Keyboard.Q,
    Phaser.Keyboard.D,
    Phaser.Keyboard.E,
    ...numberToPhaserKey.values(),
]);
