import {IGameSettings, IPlayerInfo} from "../../protocol/sharedInterfaces";
import {Character} from "./Character";
import {CommandBuffer} from "./CommandBuffer";
import {PlayerCommand} from "./Commands";
import {GoldenTrident} from "./GoldenTrident";
import {PlayingState} from "./PlayingGameState";
import {SettingGameState} from "./SettingGameState";

export class Game extends Phaser.Game {
    public socket: SocketIOClient.Socket;

    private _initialPlayerInfos = new Array<IPlayerInfo>();
    private _characterList = new Map<string, Character>();

    private _clientId: string;

    private _goldenTrident: GoldenTrident;

    private _tickRate: number;
    private _remainingTimeForUpdatingMS = 0;
    private _speedRatio = 1;

    private _debuggingActivated = false;

    private _commandBuffer: CommandBuffer;

    constructor(socket: SocketIOClient.Socket,
                commandBuffer: CommandBuffer,
                debuggingActivated: boolean = false) {
        super("100%", "100%", Phaser.AUTO, "game");
        this.socket = socket;
        this._commandBuffer = commandBuffer;
        this._debuggingActivated = debuggingActivated;
        this.state.add("SettingGameState", SettingGameState, true);
        this.state.add("PlayingState", PlayingState, false);
    }

    get initialPlayerInfos(): any[] {
        return this._initialPlayerInfos;
    }

    get characterList(): Map<string, Character> {
        return this._characterList;
    }

    get clientId(): string {
        return this._clientId;
    }

    get goldenTrident(): GoldenTrident {
        return this._goldenTrident;
    }

    set goldenTrident(value: GoldenTrident) {
        this._goldenTrident = value;
    }

    get remainingTimeForUpdatingMS(): number {
        return this._remainingTimeForUpdatingMS;
    }

    set remainingTimeForUpdatingMS(value: number) {
        this._remainingTimeForUpdatingMS = Math.max(value, 0);
    }

    get speedRatio(): number {
        return this._speedRatio;
    }

    set speedRatio(value: number) {
        this._speedRatio = value;
    }

    get tickRate(): number {
        return this._tickRate;
    }

    get player() {
        return this._characterList.get(this._clientId);
    }

    get debuggingActivated(): boolean {
        return this._debuggingActivated;
    }

    get commandBuffer(): CommandBuffer {
        return this._commandBuffer;
    }

    public startGame(gameSettings: IGameSettings) {
        console.log("Starting game.");
        this._initialPlayerInfos = gameSettings.initialPlayerInfos;
        this._clientId = gameSettings.controlledclientId;
        this._tickRate = gameSettings.tickRate;
        this.state.start("PlayingState");
    }

    public startNextRound(commandsForThisRound: PlayerCommand[]) {
        while (commandsForThisRound.length > 0) {
            commandsForThisRound.shift().handleCommand();
        }
        this._reinitializeRemainingTimeForUpdating();
    }

    private _reinitializeRemainingTimeForUpdating() {
        console.assert(this._remainingTimeForUpdatingMS === 0);
        this._remainingTimeForUpdatingMS =
            this._tickRate * this._speedRatio;
    }
}
