// tslint:disable:max-classes-per-file
import {Sex} from "../../protocol/sharedInterfaces";
import {Appearance} from "./Appearance";
import {
    ArcherAppearance,
    ClassAppearance,
    RaceAppearance,
    WarriorAppearance,
    WizardAppearance,
} from "./AppearanceParts";
import {Game} from "./Game";
import {ICursor} from "./interfaces";
import {ModularCharacterSprite} from "./ModularCharacterSprite";
import {Skill, SkillBattleCry, SkillCharge, SkillCure, SkillManaReg,
    SkillNightmare, SkillParalyzingArrow, SkillPoisonedArrow,
    SkillPunchingArrow, SkillResurection, SkillRunePrison,
    SkillSprint, SkillUnbreakable} from "./Skill";
import {Weapon} from "./Weapon";

interface ICharacterClass {
    readonly name: string;
    readonly autoAttackAmmo: string;
    readonly autoAttackDamage: number;
    readonly autoAttackSpeed: number;
    readonly scopeRadius: number;
    readonly maxHealth: number;
    readonly maxSpeed: number;
    readonly maxEnergy: number;
    readonly skills: Skill[];
}

export abstract class Character extends ModularCharacterSprite {
    protected _characterClass: ICharacterClass;
    protected _currentAutoAttackDamage: number;
    protected _currentAutoAttackSpeed: number;
    protected _currentEnergy: number;
    protected _currentShield: number;
    protected _currentSpeed: number;
    private _name: string;
    private _cursor: ICursor = {
        down: false,
        left: false,
        right: false,
        up: false,
    };
    private _target: Character;
    private _isUsingSkill: boolean = false;
    private _lifeText: Phaser.Text;
    private _teamId: number;

    private _weapon: Weapon;

    constructor(game: Phaser.Game,
                x: number, y: number,
                appearance: Appearance,
                characterName: string,
                teamId: number) {
        super(game, x, y, appearance);
        this._name = characterName;
        this._teamId = teamId;
        this._target = this;

        this._currentEnergy = 50;
        this._currentShield = 0;
        this._isUsingSkill = false;

        this._weapon = new Weapon(this);

        this._lifeText = this.game.add.text(0, 0, "", {});
        this._lifeText.anchor.set(0.5, 1.2);
        this.addChild(this._lifeText);

        this.heal(this.maxHealth);
    }

    get name(): string {
        return this._name;
    }

    set target(value: Character) {
        this._target = value;
    }

    get target(): Character {
        return this._target;
    }

    get cursor(): ICursor {
        return this._cursor;
    }

    set cursor(value: ICursor) {
        this._cursor = value;
    }

    get currentAutoAttackDamage(): number {
        return this._currentAutoAttackDamage;
    }

    set currentAutoAttackDamage(value: number) {
        this._currentAutoAttackDamage = value;
    }

    get currentAutoAttackSpeed(): number {
        return this._currentAutoAttackSpeed;
    }

    set currentAutoAttackSpeed(value: number) {
        this._currentAutoAttackSpeed = value;
    }

    get currentEnergy(): number {
        return this._currentEnergy;
    }

    set currentEnergy(value: number) {
        this._currentEnergy =
            Math.min(value, this._characterClass.maxEnergy);
    }

    get currentShield(): number {
        return this._currentShield;
    }

    set currentShield(value: number) {
        this._currentShield = value;
    }

    get currentSpeed(): number {
        return this._currentSpeed;
    }

    set currentSpeed(value: number) {
        if (value !== 0) {
            this._currentSpeed =
                Math.max(value, this._characterClass.maxSpeed);
        } else {
            this._currentSpeed = value;
        }
    }

    get isUsingSkill(): boolean {
        return this._isUsingSkill;
    }

    set isUsingSkill(value: boolean) {
        this._isUsingSkill = value;
        if (this._isUsingSkill) {
            this.play("shot");
        } else {
            this.animations.stop();
        }
    }

    get characterClass(): ICharacterClass {
        return this._characterClass;
    }

    get teamId(): number {
        return this._teamId;
    }

    public updateForElaspedTime(elaspedMS: number) {
        super.updateForElaspedTime(elaspedMS);

        if (!this.alive) {
            return;
        }

        for (const skill of this.characterClass.skills) {
            skill.updateForElaspedTime(this, elaspedMS);
        }

        if (this._isUsingSkill) {
            this.play("skill");
            this.body.velocity.x = 0.0;
            this.body.velocity.y = 0.0;
            return;
        }

        this._weapon.updateForElaspedTime(elaspedMS);

        let speed = new Phaser.Point(0, 0); // speed vector
        let animationToPlay: string;

        if (this._cursor.up) {
            speed.y -= 1;
            animationToPlay = "up";
        }
        if (this._cursor.down) {
            speed.y += 1;
            animationToPlay = "down";
        }
        if (this._cursor.left) {
            speed.x -= 1;
            animationToPlay = "left";
        }
        if (this._cursor.right) {
            speed.x += 1;
            animationToPlay = "right";
        }

        if (speed.getMagnitude() === 0.0) {
            this.animations.stop();
        } else {
            this.play(animationToPlay);
        }

        speed = Phaser.Point.normalize(speed);
        speed.x *= this._currentSpeed * 1e-1;
        speed.y *= this._currentSpeed * 1e-1;

        this.body.velocity.x = speed.x * elaspedMS;
        this.body.velocity.y = speed.y * elaspedMS;
    }

    public useSkill(skillIndex: number) {
        if (skillIndex >= this._characterClass.skills.length) {
            console.warn("bad skillIndex, nothing happened");
            return;
        }
        const skill = this._characterClass.skills[skillIndex];
        skill.apply(this);
    }

    public damage(amount: number): Phaser.Sprite {
        if (!this.alive) {
            return;
        }
        if (this.currentShield !== 0) {
            if ((this.currentShield - amount) < 0) {
                amount -= amount - this.currentShield;
                this.currentShield = 0;
            } else {
                this.currentShield -= amount;
                amount = 0;
            }
        }
        this.play("slash_down");
        const sprite = super.damage(amount);
        this._updateLifeText();
        return sprite;
    }

    public decrease_damage(coef: number): void {
        if (!this.alive) {
            return;
        }

        this.currentAutoAttackDamage /= coef;
    }

    public heal(amount: number): Phaser.Sprite {
        if (!this.alive) {
            return;
        }
        if (this.health <= 0) {
            this.health = 0;
        }
        const sprite = super.heal(amount);
        this._updateLifeText();
        return sprite;
    }

    public increase_damage(coef: number): void {
        if (!this.alive) {
            return;
        }

        this.currentAutoAttackDamage *= coef;
    }

    public kill(): Phaser.Sprite {
        const game = <Game> this.game;
        game.goldenTrident.pointsManager.addPointsAgainst(this.teamId, 10);
        return super.kill();
    }

    public mana(amount: number): void {
        if (!this.alive) {
            return;
        }
        this.currentEnergy =
            Math.min(this.currentEnergy + amount,
                this._characterClass.maxEnergy);
    }

    public sprint(coef: number): void {
        if (!this.alive) {
            return;
        }
        this.currentSpeed += this.currentSpeed * coef;
    }

    public slow(coef: number): void {
        if (!this.alive) {
            return;
        }
        this.currentSpeed -= this.currentSpeed * coef;
    }

    public stun(): void {
        if (!this.alive) {
            return;
        }
        this.currentSpeed = 0;
        this._isUsingSkill = true;
    }

    public unstun(): void {
        if (!this.alive) {
            return;
        }
        this.currentSpeed = this._characterClass.maxSpeed;
        this._isUsingSkill = false;
    }

    public destroy(destroyChildren?: boolean): void {
        this._lifeText.destroy();
        super.destroy(destroyChildren);
    }

    private _updateLifeText() {
        if (!this._lifeText) {
            return;
        }

        const health = Math.round(this.health);
        const healthText = String(health > 0 ? health : "");

        let lifeRatio = Math.floor(health / this.maxHealth * 100);

        // TODO: use health percentage (current/max) instead
        let lifeTextColor;
        if (lifeRatio > 50) {
            lifeTextColor = "green";
        } else if (lifeRatio > 15) {
            lifeTextColor = "orange";
        } else {
            lifeTextColor = "red";
        }

        this._lifeText.setText(healthText);
        this._lifeText.setStyle({font: "2.5em;", fill: lifeTextColor});
    }
}

export class ClassCharacter extends Character {
    constructor(game: Phaser.Game,
                initialX: number,
                initialY: number,
                characterName: string,
                teamId: number,
                sex: Sex,
                raceAppearance: RaceAppearance,
                classAppearance: ClassAppearance) {
        const appearance =
            new Appearance(classAppearance, raceAppearance, sex);
        super(
            game,
            initialX,
            initialY,
            appearance,
            characterName,
            teamId);
    }
}

export class WarriorCharacter extends ClassCharacter {
    constructor(game: Phaser.Game,
                initialX: number,
                initialY: number,
                characterName: string,
                teamId: number,
                raceAppearance: RaceAppearance,
                sex: Sex) {
        super(game,
            initialX, initialY,
            characterName,
            teamId,
            sex,
            raceAppearance,
            new WarriorAppearance());

        const skills =
            new Array<Skill>(
                new SkillCharge(),
                new SkillUnbreakable(),
                new SkillBattleCry(),
                new SkillSprint(),
            );

        this._characterClass = {
            autoAttackAmmo: "img/ammo/arrow.png",
            autoAttackDamage: 5,
            autoAttackSpeed: 1.2,
            maxEnergy: 200,
            maxHealth: 350,
            maxSpeed: 200,
            name: "Warrior",
            scopeRadius: 100,
            skills,
        };

        this.currentAutoAttackDamage = this._characterClass.autoAttackDamage;
        this.currentAutoAttackSpeed = this._characterClass.autoAttackSpeed;
        this.currentEnergy = this._characterClass.maxEnergy;
        this.currentSpeed = this._characterClass.maxSpeed;
        this.maxHealth = this._characterClass.maxHealth;
        this.heal(this.maxHealth);
    }
}

export class ArcherCharacter extends ClassCharacter {
    constructor(game: Phaser.Game,
                initialX: number,
                initialY: number,
                characterName: string,
                teamId: number,
                raceAppearance: RaceAppearance,
                sex: Sex) {
        super(game,
            initialX, initialY,
            characterName,
            teamId,
            sex,
            raceAppearance,
            new ArcherAppearance());

        const skills =
            new Array<Skill>(
                new SkillPunchingArrow(),
                new SkillParalyzingArrow(),
                new SkillPoisonedArrow(),
                new SkillCure());

        this._characterClass = {
            autoAttackAmmo: "img/ammo/arrow.png",
            autoAttackDamage: 5,
            autoAttackSpeed: 1.2,
            maxEnergy: 200,
            maxHealth: 275,
            maxSpeed: 150,
            name: "Archer",
            scopeRadius: 200,
            skills,
        };

        this.currentAutoAttackDamage = this._characterClass.autoAttackDamage;
        this.currentAutoAttackSpeed = this._characterClass.autoAttackSpeed;
        this.currentEnergy = this._characterClass.maxEnergy;
        this.currentSpeed = this._characterClass.maxSpeed;
        this.maxHealth = this._characterClass.maxHealth;
        this.heal(this.maxHealth);
    }
}

export class WizardCharacter extends ClassCharacter {
    constructor(game: Phaser.Game,
                initialX: number,
                initialY: number,
                characterName: string,
                teamId: number,
                raceAppearance: RaceAppearance,
                sex: Sex) {
        super(game,
            initialX, initialY,
            characterName,
            teamId,
            sex,
            raceAppearance,
            new WizardAppearance());

        const skills =
            new Array<Skill>(
                new SkillNightmare(),
                new SkillRunePrison(),
                new SkillManaReg(),
                new SkillResurection());

        this._characterClass = {
            autoAttackAmmo: "img/ammo/arrow.png",
            autoAttackDamage: 5,
            autoAttackSpeed: 2,
            maxEnergy: 300,
            maxHealth: 250,
            maxSpeed: 150,
            name: "Wizard",
            scopeRadius: 200,
            skills,
        };

        this.currentAutoAttackDamage = this._characterClass.autoAttackDamage;
        this.currentAutoAttackSpeed = this._characterClass.autoAttackSpeed;
        this.currentEnergy = this._characterClass.maxEnergy;
        this.currentSpeed = this._characterClass.maxSpeed;
        this.maxHealth = this._characterClass.maxHealth;
        this.heal(this.maxHealth);
    }
}
