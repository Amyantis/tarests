import {WeaponTrident} from "./appearanceSkins";
import {Character} from "./Character";
import {Game} from "./Game";
import {PointsManager} from "./PointsManager";

export class GoldenTrident {
    private _game: Game;
    private _pointsManager: PointsManager;
    private _player: Character = null;
    private _weaponTrident: WeaponTrident;
    private _distanceToBeCatched: number = 90;

    constructor(game: Game,
                x: number, y: number,
                pointsManager: PointsManager) {
        this._game = game;
        this._pointsManager = pointsManager;
        this._weaponTrident = new WeaponTrident(game, WeaponTrident.file);
        this._weaponTrident.position = new Phaser.Point(x, y);
    }

    get player(): Character {
        return this._player;
    }

    set player(character: Character) {
        const isCharacterEmpty =
            character === null || typeof character === "undefined";
        if (this.isCarried() && isCharacterEmpty) {
            // player drops trident
            const currentPlayerPosition = this._player.position;

            this._player.removeChild(this._weaponTrident);
            this._player.weaponVisible = true;
            this._player = null;

            const game = this._weaponTrident.game;
            this._weaponTrident.destroy();
            this._weaponTrident = new WeaponTrident(game, WeaponTrident.file);
            this._weaponTrident.position.x = currentPlayerPosition.x;
            this._weaponTrident.position.y = currentPlayerPosition.y;

            return;
        }

        if (!this.isCarried()) {
            const distanceBetween =
                this._game.physics.arcade.distanceBetween(
                    character, this._weaponTrident);
            const isTooFar = distanceBetween > this._distanceToBeCatched;
            if (isTooFar) {
                return;
            }

            this._player = character;
            this._player.weaponVisible = false;
            this._player.addChild(this._weaponTrident);
            this._weaponTrident.position = new Phaser.Point(0, 0);
            this._weaponTrident.holder = this._player;
            return;
        }
    }

    get sprite(): WeaponTrident {
        return this._weaponTrident;
    }

    get pointsManager(): PointsManager {
        return this._pointsManager;
    }

    public isCarried() {
        return this._player !== null && typeof this._player !== "undefined";
    }

    public updateForElaspedTime(elaspedMS: number) {
        if (this.isCarried() && !this._player.alive) {
            this._player.weaponVisible = true;
            this.player = null;
        }

        if (this.isCarried()) {
            this._weaponTrident.update();

            for (const character of this._game.characterList.values()) {
                if (character.teamId === this._player.teamId) {
                    const distanceBetween =
                        this._game.physics.arcade.distanceBetween(
                            character, this._player);
                    if (distanceBetween < this._distanceToBeCatched) {
                        const regenerationRatePerSeconds = 3 * 1e-3;
                        character.heal(regenerationRatePerSeconds * elaspedMS);
                    }
                }
            }
            const pointsToAdd = 3 * elaspedMS * 1e-3;
            this._pointsManager.addPoints(this.player.teamId, pointsToAdd);
        }
    }
}
