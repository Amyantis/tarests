import {Appearance} from "./Appearance";

export class ModularCharacterSprite extends Phaser.Sprite {
    private _appearance: Appearance;

    constructor(game: Phaser.Game,
                initialX: number,
                initialY: number,
                appearance: Appearance) {
        super(game,
            initialX, initialY,
            appearance.bodyClass.correspondingFile(appearance),
            130);

        this.anchor.set(0.5, 0.5);

        // TODO: manage body with Phaser.Group
        this._setBody();

        this._setAnimation();

        this._appearance = appearance;
        this._appearance.attach(this);
    }

    get appearance(): Appearance {
        return this._appearance;
    }

    set weaponVisible(value: boolean) {
        this._appearance.weaponVisible = value;
    }

    public updateForElaspedTime(elaspedMS: number) {
        super.update();
        this._appearance.updateForElaspedTime(elaspedMS);
    }

    private _setBody() {
        this.game.physics.enable(this, Phaser.Physics.ARCADE);
        this.body.collideWorldBounds = true;
        this.body.setSize(25, 13, 20, 49);
        this.body.allowRotation = false;
    }

    private _setAnimation() {
        // TODO: simplify animations with intervals

        this.animations.add("skill_up",
            [1, 2, 3, 4, 5, 6], 10, true);
        this.animations.add("skill_left",
            [13, 14, 15, 16, 17, 18, 19], 10, true);
        this.animations.add("skill_down",
            [26, 27, 28, 29, 30, 31, 32], 10, true);
        this.animations.add("skill_right",
            [39, 40, 41, 42, 43, 44, 45], 10, true);

        this.animations.add("skill",
            [26, 27, 28, 29, 30, 31, 32], 10, true);

        this.animations.add("thrush_up",
            [52, 53, 54, 55, 56, 57, 58, 59], 15, true);
        this.animations.add("thrush_left",
            [65, 66, 67, 68, 69, 70, 71, 72], 15, true);
        this.animations.add("thrush_down",
            [78, 79, 80, 81, 82, 83, 84, 85], 15, true);
        this.animations.add("thrush_right",
            [91, 92, 93, 94, 95, 96, 97, 98], 15, true);

        this.animations.add("up",
            [104, 105, 106, 107, 108, 109, 110, 111, 112], 20, true);
        this.animations.add("left",
            [117, 118, 119, 120, 121, 122, 123, 124, 125], 20, true);
        this.animations.add("down",
            [130, 131, 132, 133, 134, 135, 136, 137, 138], 20, true);
        this.animations.add("right",
            [143, 144, 145, 146, 147, 148, 149, 150, 151], 20, true);

        this.animations.add("slash_up",
            [156, 157, 158, 159, 160, 161], 6, false);
        this.animations.add("slash_left",
            [169, 170, 171, 172, 173, 174], 6, false);
        this.animations.add("slash_down",
            [182, 183, 184, 185, 186, 187], 6, false);
        this.animations.add("slash_right",
            [195, 196, 197, 198, 199, 200], 6, false);

        this.animations.add("shoot_up",
            [208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220],
            13, true);
        this.animations.add("shoot_left",
            [221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233],
            13, true);
        this.animations.add("shoot_down",
            [234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246],
            13, true);
        this.animations.add("shoot_right",
            [247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259],
            13, true);

        this.animations.add("death", [260, 261, 262, 263, 264, 265], 20, false);

        this.events.onKilled.add(() => {
            this.exists = true;
            this.play("death");
        });
    }
}
