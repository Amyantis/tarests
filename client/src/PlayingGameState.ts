import {IPlayerInfo} from "../../protocol/sharedInterfaces";
import {
    HumanAppearance,
    LightAppearance,
    MetalAppearance,
    OrcAppearance,
} from "./AppearanceParts";
import {ArcherCharacter, WarriorCharacter, WizardCharacter} from "./Character";
import {GameState} from "./GameState";
import {GoldenTrident} from "./GoldenTrident";
import {listenedKeys, numberToPhaserKey} from "./keys";
import {Minimap} from "./Minimap";
import {PointsManager} from "./PointsManager";
import {UserInterface} from "./UserInterface";
import {debugVars} from "./utils";

export class PlayingState extends GameState {
    private _layers = new Map<string, Phaser.TilemapLayer>();

    private _minimap: Minimap;
    private _userInterface: UserInterface;

    private _teams = new Map<number, Phaser.Group>();

    private _pointsManager: PointsManager;

    private _goldenTrident: GoldenTrident;

    public create() {
        this.game.time.advancedTiming = true;

        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.physics.arcade.skipQuadTree = false;

        // prevent delay when window loose focus
        this.game.stage.disableVisibilityChange = true;

        this._setTilemap();

        this._attachKeysListener();

        // spawn each player at its initial position
        for (const initialPlayerInfo of this.game.initialPlayerInfos) {
            this._spawnPlayer(initialPlayerInfo);
        }

        this._layers.get("treetop").bringToTop();

        this.game.camera.follow(this.game.player);

        this._pointsManager = new PointsManager(this.game, this._teams);

        this._goldenTrident =
            new GoldenTrident(this.game, 1581, 1345, this._pointsManager);
        this.game.goldenTrident = this._goldenTrident;

        this._minimap =
            new Minimap(
                this.game,
                this.game.player,
                this._teams,
                this._goldenTrident,
                17);
        this._userInterface =
            new UserInterface(
                this.game.player,
                this._teams.get(this.game.player.teamId),
                this._sendSkillUseEvent.bind(this),
                this._pointsManager);
    }

    public update() {
        for (const sprite of this.game.characterList.values()) {
            sprite.body.velocity.x = 0;
            sprite.body.velocity.y = 0;
        }

        // TODO: optimize following collisions (doing 2 times the same thg)
        for (const i of this._teams.values()) {
            for (const j of this._teams.values()) {
                this.physics.arcade.collide(i, j);
            }
            this.game.physics.arcade.collide(
                i, this._layers.get("collision"));
        }

        this._userInterface.update();
        this._minimap.update();

        // handle player commands if there is time for it
        if (this.game.remainingTimeForUpdatingMS > 0) {
            let elaspedMS =
                Math.round(this.game.time.elapsedMS * this.game.speedRatio);
            if (this.game.remainingTimeForUpdatingMS - elaspedMS < 0) {
                elaspedMS = this.game.remainingTimeForUpdatingMS;
            }

            this.game.remainingTimeForUpdatingMS -= elaspedMS;

            this._updateForElaspedTime(elaspedMS);

            if (this.game.remainingTimeForUpdatingMS <= 0) {
                this.game.socket.emit("setRoundAsProcessed");
                if (this.game.commandBuffer.hasARoundToProcess()) {
                    const commandsForNextRound =
                        this.game.commandBuffer.getCommandsForNextRound();
                    this.game.startNextRound(commandsForNextRound);
                }
            }
        }
    }

    public render() {
        for (const sprite of this.game.characterList.values()) {
            const color =
                sprite.teamId === this.game.player.teamId ?
                    "rgba(0,255,0,0.4)" : "rgba(255,0,0,0.4)";
            const scopeCircle = new Phaser.Circle(
                sprite.position.x,
                sprite.position.y + sprite.height / 2 + 9,
                10);
            this.game.debug.geom(scopeCircle, color, true);
        }

        this._renderTarget();
        this._renderScope();

        if (this.game.debuggingActivated) {
            this._renderDebugInfo();
        }
    }

    private _renderDebugInfo() {
        this.game.debug.bodyInfo(this.game.player, 32, 32, "blue");
        this.game.debug.body(this.game.player, "blue");
        this.game.debug.cameraInfo(this.camera, 32, 230);
        this._layers.get("collision").debug = true;
        this.game.debug.quadTree(this.game.physics.arcade.quadTree);

        const infos = [
            ["speedRatio", this.game.speedRatio.toFixed(3)],
            ["fps", this.game.time.fps],
        ];
        debugVars(infos, 32, 320, this.game, "yellow");
    }

    private _renderScope() {
        const scopeCircle = new Phaser.Circle(
            this.game.player.position.x,
            this.game.player.position.y,
            2 * this.game.player.characterClass.scopeRadius);
        this.game.debug.geom(scopeCircle, "rgba(0,255,255,0.2)", false);
    }

    private _renderTarget() {
        const width = this.game.player.target.width + 40;
        const height = this.game.player.target.height + 40;
        const targetRectangle =
            new Phaser.Rectangle(
                this.game.player.target.x - width / 2,
                this.game.player.target.y - height / 2,
                width,
                height);
        const color =
            this.game.player.target.teamId === this.game.player.teamId ?
                "rgba(0,255,0,0.4)" : "rgba(255,0,0,0.4)";
        this.game.debug.geom(targetRectangle, color, false);
    }

    private _updateForElaspedTime(elaspedMS: number) {
        this._goldenTrident.updateForElaspedTime(elaspedMS);

        this._regeneratePlayerForElaspedTime(elaspedMS);

        for (const sprite of this.game.characterList.values()) {
            sprite.updateForElaspedTime(elaspedMS);
        }
    }

    private _setTilemap() {
        const tilemap = this.game.add.tilemap("map/map.json");

        tilemap.addTilesetImage("campfire", "map/campfire.png");
        tilemap.addTilesetImage("collision", "map/collision.png");
        tilemap.addTilesetImage("terrain", "map/terrain.png");
        tilemap.addTilesetImage("treetop", "map/treetop.png");
        tilemap.addTilesetImage("trunk", "map/trunk.png");

        const layerList = [
            "collision",
            "ground",
            "items",
            "treetop",
            "trunk",
        ];
        for (const layerName of layerList) {
            this._layers.set(layerName, tilemap.createLayer(layerName));
        }

        const collisionLayer = this._layers.get("collision");

        collisionLayer.resizeWorld();
        tilemap.setCollisionBetween(1, 10000, true, collisionLayer);
        this.game.physics.arcade.enable(collisionLayer);
        collisionLayer.visible = false;
    }

    private _attachKeysListener() {
        for (const listenedKey of listenedKeys.values()) {
            const key =
                this.game.input.keyboard.addKey(listenedKey);
            key.onDown.add(
                this._sendKeyEventToServer.bind(
                    this, listenedKey, true), this);
            key.onUp.add(
                this._sendKeyEventToServer.bind(
                    this, listenedKey, false), this);
        }
    }

    private _spawnPlayer(initialPlayerInfo: IPlayerInfo) {
        // TODO: move classesAvailable away
        const classesAvailable =
            [ArcherCharacter, WarriorCharacter, WizardCharacter];
        const racesAvailable =
            [HumanAppearance, LightAppearance, OrcAppearance, MetalAppearance];
        const classIndex =
            initialPlayerInfo.clientInfo.classId % classesAvailable.length;
        const characterClassConstructor = classesAvailable[classIndex];

        const raceIndex =
            initialPlayerInfo.clientInfo.teamId % racesAvailable.length;
        const raceAppearanceConstructor = racesAvailable[raceIndex];
        const raceAppearance = new raceAppearanceConstructor();

        const newCharacter =
            new characterClassConstructor(
                this.game,
                initialPlayerInfo.initialPosition.x,
                initialPlayerInfo.initialPosition.y,
                initialPlayerInfo.clientInfo.clientAlias,
                initialPlayerInfo.clientInfo.teamId,
                raceAppearance,
                initialPlayerInfo.clientInfo.sex,
            );

        newCharacter.inputEnabled = true; // allow us to get events onInput
        newCharacter.events.onInputDown.add(() => {
                console.log(
                    "Clicked on player",
                    initialPlayerInfo.clientInfo.clientId);
                this.game.socket.emit(
                    "broadcastTargetSelectionEvent",
                    initialPlayerInfo.clientInfo.clientId);
            },
            this,
        );

        this.game.characterList.set(
            initialPlayerInfo.clientInfo.clientId,
            newCharacter);

        this._addPlayerToItsTeam(newCharacter);
    }

    private _addPlayerToItsTeam(newCharacter: any) {
        if (!this._teams.has(newCharacter.teamId)) {
            const physicsGroup =
                this.game.add.physicsGroup(Phaser.Physics.ARCADE);
            this._teams.set(newCharacter.teamId, physicsGroup);
        }

        const group = this._teams.get(newCharacter.teamId);
        group.add(newCharacter);
    }

    private _regeneratePlayerForElaspedTime(elaspedTimeMS: number) {
        const amountForThisPeriod = elaspedTimeMS * 1e-3;

        for (const character of this.game.characterList.values()) {
            if (!character.alive || character.health <= 0) {
                return;
            }
            character.heal(amountForThisPeriod);
            character.currentEnergy += amountForThisPeriod;
        }
    }

    private _sendKeyEventToServer(keyId: number, isDown: boolean = true) {
        const keyEventFromClient = {keyId, isDown};
        this.game.socket.emit("broadcastKeyEvent", keyEventFromClient);
    }

    private _sendSkillUseEvent(skillId: number) {
        const phaserKey =
            numberToPhaserKey.get(skillId);
        this._sendKeyEventToServer(phaserKey);
    }
}
