import {appearanceSkins} from "./appearanceSkins";
import {GameState} from "./GameState";

export class SettingGameState extends GameState {
    public preload() {
        for (const appearanceSkin of appearanceSkins) {
            const neededRessources =
                appearanceSkin.neededRessources;
            for (const file of neededRessources.filesPaths) {
                const relativeFilePath = "img/lcp_spritesheet/" + file;
                this.load.spritesheet(
                    file, relativeFilePath,
                    neededRessources.frameSize.width,
                    neededRessources.frameSize.height);
            }
        }

        this.game.load.spritesheet(
            "img/ammo/rgblaser.png",
            "img/ammo/rgblaser.png",
            4, 4);

        this.game.load.tilemap(
            "map/map.json", "map/map.json",
            null, Phaser.Tilemap.TILED_JSON);

        this.game.load.image("map/campfire.png", "map/campfire.png");
        this.game.load.image("map/collision.png", "map/collision.png");
        this.game.load.image("map/terrain.png", "map/terrain.png");
        this.game.load.image("map/treetop.png", "map/treetop.png");
        this.game.load.image("map/trunk.png", "map/trunk.png");

        // TODO: display a loading bar
    }
}
