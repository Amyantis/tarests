// tslint:disable:max-classes-per-file
// tslint:disable:object-literal-sort-keys

import {
    Ammo,
    AmmoArrow,
    Armor,
    ArmorChestGold,
    ArmorChestLeather,
    ArmorChestPlate,
    Arms,
    ArmsGold,
    ArmsPlate,
    Belts,
    Body,
    BodyLight,
    BodyOrc,
    Bracelet,
    Bracers,
    Buckles,
    Cape,
    CapeBrown,
    CapeGray,
    CapeYellow,
    Clothes,
    Ears,
    Eyes,
    Gloves,
    GlovesGold,
    GlovesMetal,
    Greaves,
    GreavesGold,
    GreavesMetal,
    Hair,
    HairBlonde,
    HairBrown,
    HairGray,
    Hat,
    HatChain,
    HatGold,
    HatMetal,
    Jacket,
    Mail,
    Necklaces,
    Nose,
    Quiver,
    Shield,
    Shoes,
    ShoesGold,
    ShoesMetal,
    Shoulders,
    Spikes,
    Tie,
    Weapon,
    WeaponBow,
    WeaponSpear,
} from "./appearanceSkins";
import {ModularCharacterSprite} from "./ModularCharacterSprite";

interface IRaceAttributes {
    armor: typeof Armor;
    arms: typeof Arms;
    belts: typeof Belts;
    body: typeof Body;
    bracelet: typeof Bracelet;
    bracers: typeof Bracers;
    buckles: typeof Buckles;
    cape: typeof Cape;
    clothes: typeof Clothes;
    ears: typeof Ears;
    eyes: typeof Eyes;
    gloves: typeof Gloves;
    greaves: typeof Greaves;
    hat: typeof Hat;
    hair: typeof Hair;
    jacket: typeof Jacket;
    mail: typeof Mail;
    necklaces: typeof Necklaces;
    nose: typeof Nose;
    quiver: typeof Quiver;
    shoes: typeof Shoes;
    shoulders: typeof Shoulders;
    spikes: typeof Spikes;
    tie: typeof Tie;
}

interface IClassAttributes {
    ammo: typeof Ammo;
    hat: typeof Hat;
    shield: typeof Shield;
    weapon: typeof Weapon;
    quiver: typeof Quiver;
}

// TODO: there are unusefully public members in those classes

abstract class AppearancePart {
    public abstract attributes: any;
    public specialAttributes = new Set<string>();

    get attributesKeys() {
        return new Set(Object.keys(this.attributes));
    }

    public attach(character: ModularCharacterSprite) {
        for (const attributeKey of this.attributesKeys) {
            if (!this.specialAttributes.has(attributeKey) &&
                this.attributes[attributeKey] !== null) {
                this[attributeKey] =
                    this.attributes[attributeKey].attach(character);
            }
        }
    }

    public updateForElaspedTime(elaspedMS: number) {
        for (const attributeKey of this.attributesKeys) {
            if (typeof this[attributeKey] !== "undefined") {
                this[attributeKey].update();
            }
        }
    }
}

export abstract class RaceAppearance extends AppearancePart {
    public abstract attributes: IRaceAttributes;
    public specialAttributes = new Set(["body"]);

    public armor: Armor;
    public arms: Arms;
    public belts: Belts;
    public body: Body;
    public bracelet: Bracelet;
    public bracers: Bracers;
    public buckles: Buckles;
    public cape: Cape;
    public clothes: Clothes;
    public ears: Ears;
    public eyes: Eyes;
    public gloves: Gloves;
    public greaves: Greaves;
    public hair: Hair;
    public hat: Hat;
    public jacket: Jacket;
    public mail: Mail;
    public necklaces: Necklaces;
    public nose: Nose;
    public quiver: Quiver;
    public shoes: Shoes;
    public shoulders: Shoulders;
    public spikes: Spikes;
    public tie: Tie;
}

export class OrcAppearance extends RaceAppearance {
    public attributes: IRaceAttributes = {
        mail: null,
        armor: ArmorChestLeather,
        arms: null,
        belts: null,
        body: BodyOrc,
        bracelet: null,
        bracers: null,
        buckles: null,
        clothes: null,
        ears: null,
        eyes: null,
        gloves: null,
        greaves: null,
        hair: null,
        hat: null,
        jacket: null,
        necklaces: null,
        nose: null,
        quiver: null,
        shoes: null,
        shoulders: null,
        spikes: null,
        tie: null,
        cape: CapeBrown,
    };
}

export class HumanAppearance extends RaceAppearance {
    public attributes: IRaceAttributes = {
        mail: Mail,
        armor: ArmorChestLeather,
        arms: null,
        belts: null,
        body: BodyLight,
        bracelet: null,
        bracers: null,
        buckles: null,
        clothes: null,
        ears: null,
        eyes: null,
        gloves: null,
        greaves: GreavesMetal,
        hair: null,
        hat: HatChain,
        jacket: null,
        necklaces: null,
        nose: null,
        quiver: null,
        shoes: ShoesMetal,
        shoulders: null,
        spikes: null,
        tie: null,
        cape: CapeBrown,
    };
}

export class LightAppearance extends RaceAppearance {
    public attributes: IRaceAttributes = {
        mail: null,
        armor: ArmorChestGold,
        arms: ArmsGold,
        belts: null,
        body: BodyLight,
        bracelet: null,
        bracers: null,
        buckles: null,
        clothes: null,
        ears: null,
        eyes: null,
        gloves: GlovesGold,
        greaves: GreavesGold,
        hair: null,
        hat: HatGold,
        jacket: null,
        necklaces: null,
        nose: null,
        quiver: null,
        shoes: ShoesGold,
        shoulders: null,
        spikes: null,
        tie: null,
        cape: CapeYellow,
    };
}

export class MetalAppearance extends RaceAppearance {
    public attributes: IRaceAttributes = {
        mail: null,
        armor: ArmorChestPlate,
        arms: ArmsPlate,
        belts: null,
        body: BodyLight,
        bracelet: null,
        bracers: null,
        buckles: null,
        clothes: null,
        ears: null,
        eyes: null,
        gloves: GlovesMetal,
        greaves: GreavesMetal,
        hair: null,
        hat: HatMetal,
        jacket: null,
        necklaces: null,
        nose: null,
        quiver: null,
        shoes: ShoesMetal,
        shoulders: null,
        spikes: null,
        tie: null,
        cape: CapeGray,
    };
}

export abstract class ClassAppearance extends AppearancePart {
    public abstract attributes: IClassAttributes;

    public ammo: Ammo;
    public hat: Hat;
    public quiver: Quiver;
    public shield: Shield;
    public weapon: Weapon;

}

export class ArcherAppearance extends ClassAppearance {
    public attributes: IClassAttributes = {
        ammo: AmmoArrow,
        hat: null,
        quiver: Quiver,
        shield: null,
        weapon: WeaponBow,
    };
}

export class WarriorAppearance extends ClassAppearance {
    public attributes: IClassAttributes = {
        ammo: null,
        hat: null,
        quiver: null,
        shield: Shield,
        weapon: WeaponSpear,
    };
}

export class WizardAppearance extends ClassAppearance {
    public attributes: IClassAttributes = {
        ammo: null,
        hat: null,
        quiver: null,
        shield: null,
        weapon: null,
    };
}
