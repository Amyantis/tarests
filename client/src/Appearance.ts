import {Sex} from "../../protocol/sharedInterfaces";
import {ClassAppearance, RaceAppearance} from "./AppearanceParts";
import {ModularCharacterSprite} from "./ModularCharacterSprite";

export class Appearance {
    private _classAppearance: ClassAppearance;
    private _raceAppearance: RaceAppearance;
    private _sex: Sex;

    constructor(classAppearance: ClassAppearance,
                raceAppearance: RaceAppearance,
                sex: Sex) {
        this._classAppearance = classAppearance;
        this._raceAppearance = raceAppearance;
        this._sex = sex;
    }

    get bodyClass() {
        return this._raceAppearance.attributes.body;
    }

    get classAppearance(): ClassAppearance {
        return this._classAppearance;
    }

    get raceAppearance(): RaceAppearance {
        return this._raceAppearance;
    }

    get sex(): Sex {
        return this._sex;
    }

    set weaponVisible(value: boolean) {
        if (typeof this._classAppearance.weapon !== "undefined") {
            this._classAppearance.weapon.visible = value;
        }
    }

    public attach(character: ModularCharacterSprite) {
        const appearanceParts =
            [this._raceAppearance, this._classAppearance];
        for (const appearancePart of appearanceParts) {
            appearancePart.attach(character);
        }
    }

    public updateForElaspedTime(elaspedMS: number) {
        const appearanceParts =
            [this._raceAppearance, this._classAppearance];
        for (const appearancePart of appearanceParts) {
            appearancePart.updateForElaspedTime(elaspedMS);
        }
    }
}
