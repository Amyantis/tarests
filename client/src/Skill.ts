// tslint:disable:max-classes-per-file
import {Character} from "./Character";
export abstract class Skill {
    public static distanceBtwnPlayerAndTarget(player: Character) {
        return player.game.physics.arcade.distanceBetween(
            player, player.target);
    }

    public static isPlayerCloseEnough(player: Character) {
        return Skill.distanceBtwnPlayerAndTarget(player) <
            player.characterClass.scopeRadius;
    }

    protected _fileIcon: string = null;
    protected _description: string = null;

    protected _canTargetMyself = false;
    protected _canTargetAlly = false;
    protected _canTargetEnnemy = true;

    private _name: string;
    private _castTime: number;
    private _coolDownTime: number;
    private _energyCost: number;
    private _lifeCost: number;
    private _hasBeenCast = false;
    private _elaspedSinceLastUsed: number;

    constructor(name: string = "Unnamed Skill",
                castTime: number = 0,
                coolDownTime: number = 0,
                energyCost: number = 0,
                lifeCost: number = 0) {
        this._name = name;
        this._castTime = castTime;
        this._coolDownTime = coolDownTime;
        this._energyCost = energyCost;
        this._lifeCost = lifeCost;

        this._elaspedSinceLastUsed = coolDownTime;
    }

    get fileIcon(): string {
        return this._fileIcon;
    }

    get description(): string {
        return this._description;
    }

    get name(): string {
        return this._name;
    }

    get castTime(): number {
        return this._castTime;
    }

    get coolDownTime(): number {
        return this._coolDownTime;
    }

    get energyCost(): number {
        return this._energyCost;
    }

    get lifeCost(): number {
        return this._lifeCost;
    }

    get elaspedSinceLastUsed(): number {
        return this._elaspedSinceLastUsed;
    }

    public updateForElaspedTime(player: Character,
                                elaspedMS: number) {
        this._elaspedSinceLastUsed += elaspedMS;

        if (this._hasBeenCast &&
            this._elaspedSinceLastUsed > this._castTime) {
            this._effects(player);
            this._endSkillUse(player);
        }
    }

    public apply(player: Character, checkTarget = true) {
        if (this._canUseSkill(player, checkTarget)) {
            player.currentEnergy -= this._energyCost;
            player.damage(this._lifeCost);

            player.isUsingSkill = true;
            this._hasBeenCast = true;

            this._elaspedSinceLastUsed = 0;
        }
    }

    protected abstract _effects(player: Character): void;

    protected _canUseSkill(player: Character, checkTarget = true) {
        if (checkTarget) {
            if (player === player.target && !this._canTargetMyself) {
                return false;
            }
            if (player.teamId === player.target.teamId &&
                !this._canTargetAlly) {
                return false;
            }
            if (player.teamId !== player.target.teamId &&
                !this._canTargetEnnemy) {
                return false;
            }
        } else {
            // Instant skill apply on player
            return !player.isUsingSkill &&
            player.currentEnergy >= this._energyCost &&
            this._elaspedSinceLastUsed - this._castTime >
            this._coolDownTime;
        }

        return !player.isUsingSkill &&
            player.currentEnergy >= this._energyCost &&
            this._elaspedSinceLastUsed - this._castTime >
            this._coolDownTime &&
            Skill.isPlayerCloseEnough(player);
    }

    private _endSkillUse(player: Character) {
        this._hasBeenCast = false;
        player.isUsingSkill = false;
    }
}

export class SkillBattleCry extends Skill {
    protected _fileIcon = "img/skills/warrior_battlecry.png";
    protected _description = `Le Guerrier entre dans une folie meurtrière et 
    double ses dégâts de base.`;

    private _repeat = -1;
    private _repeatMax = 4;
    private _coef = 2;

    constructor() {
        super("Cri de guerre", 200, 9 * Phaser.Timer.SECOND, 30);
        this._canTargetAlly = false;
        this._canTargetMyself = true;
        this._canTargetEnnemy = false;
    }

    public apply(player: Character) {
        if (this._canUseSkill(player, false)) {
            this._repeat = 0;
        }

        super.apply(player, false);
    }

    public updateForElaspedTime(player: Character,
                                elaspedMS: number): any {
        super.updateForElaspedTime(player, elaspedMS);
        if (this._repeat !== -1 && this.elaspedSinceLastUsed > this.castTime) {
            const repeat =
                Math.floor((this.elaspedSinceLastUsed - this.castTime) /
                    Phaser.Timer.SECOND);

            if (repeat === this._repeatMax) {
                this._repeat = -1;
                player.decrease_damage(this._coef);
            }

            if (this._repeat === repeat) {
                if (player.alive && this._repeat === 0) {
                    player.increase_damage(this._coef);
                }
                this._repeat++;
            }
        }
    }

    // tslint:disable-next-line:no-empty
    protected _effects(player: Character) {
    }
}

export class SkillCharge extends Skill {
    protected _fileIcon = "img/skills/warrior_charge.png";
    protected _description = `Le Guerrier charge l'ennemi, lui infligeant 60 
        points de dégâts.`;

    constructor() {
        super("Charge audacieuse", 0, 3 * Phaser.Timer.SECOND, 40);
        this._canTargetAlly = false;
        this._canTargetMyself = false;
        this._canTargetEnnemy = true;
    }

    protected _effects(player: Character) {
        player.target.damage(60);
        player.heal(20);
    }
}

export class SkillCure extends Skill {
    protected _fileIcon = "img/skills/archer_cure.png";
    protected _description = `L'archer boit une potion magique lui permettant 
        de récupérer 45 point de vie.`;

    constructor() {
        super("Potion de vie", Phaser.Timer.SECOND,
            5 * Phaser.Timer.SECOND, 40);
        this._canTargetAlly = false;
        this._canTargetMyself = true;
        this._canTargetEnnemy = false;
    }

    public apply(player: Character) {
        super.apply(player, false);
    }

    protected _effects(player: Character) {
        player.heal(45);
    }
}

export class SkillManaReg extends Skill {
    protected _fileIcon = "img/skills/wizard_manareg.png";
    protected _description = `Le Sorcier regénère le mana de sa cible de 
    60 points.`;

    constructor() {
        super("Regénération de mana", 300, 5 * Phaser.Timer.SECOND, 0, 35);
        this._canTargetMyself = true;
        this._canTargetAlly = true;
        this._canTargetEnnemy = false;
    }

    protected _effects(player: Character) {
        player.target.mana(60);
    }
}

export class SkillNightmare extends Skill {
    protected _fileIcon = "img/skills/wizard_nightmare.png";
    protected _description = `Le Sorcier frappe de terreur son ennemi 
        lui infligeant 70 points de dégâts.`;

    constructor() {
        super("Cauchemar",
            800, 2.5 * Phaser.Timer.SECOND, 75);
    }

    // tslint:disable-next-line:no-empty
    protected _effects(player: Character) {
        player.target.damage(70);
    }
}

export class SkillParalyzingArrow extends Skill {
    protected _fileIcon = "img/skills/archer_paralyzingarrow.png";
    protected _description = `L'archer tire une flèche paralysant son ennemi 
         pour une durée de 3s.`;

    private _repeat = -1;
    private _repeatMax = 3;

    constructor() {
        super("Flèche paralysante", 200, 7 * Phaser.Timer.SECOND, 70);
        this._canTargetAlly = false;
        this._canTargetMyself = false;
        this._canTargetEnnemy = true;
    }

    public apply(player: Character) {
        if (this._canUseSkill(player)) {
            this._repeat = 0;
        }

        super.apply(player);
    }

    public updateForElaspedTime(player: Character,
                                elaspedMS: number): any {
        super.updateForElaspedTime(player, elaspedMS);
        if (this._repeat !== -1 && this.elaspedSinceLastUsed > this.castTime) {
            const repeat =
                Math.floor((this.elaspedSinceLastUsed - this.castTime) /
                    Phaser.Timer.SECOND);

            if (repeat === this._repeatMax) {
                this._repeat = -1;
                player.target.unstun();
            }

            if (this._repeat === repeat) {
                this._repeat++;
                if (player.alive) {
                    player.target.stun();
                }
            }
        }
    }

    protected _effects(player: Character) {
        player.target.damage(80);
    }
}

export class SkillPoisonedArrow extends Skill {
    protected _fileIcon = "img/skills/archer_poisonedarrow.png";
    protected _description = `L'archer tire une flèche empoisonnée infligeant 
        10 points de dégâts par seconde à son ennemi pour une durée de 4s.`;

    private _repeat = -1;
    private _repeatMax = 4;

    constructor() {
        super("Flèche empoisonnée", 200, 8 * Phaser.Timer.SECOND, 60);
        this._canTargetAlly = false;
        this._canTargetMyself = false;
        this._canTargetEnnemy = true;
    }

    public apply(player: Character) {
        if (this._canUseSkill(player)) {
            this._repeat = 0;
        }

        super.apply(player);
    }

    public updateForElaspedTime(player: Character,
                                elaspedMS: number): any {
        super.updateForElaspedTime(player, elaspedMS);
        if (this._repeat !== -1 && this.elaspedSinceLastUsed > this.castTime) {
            const repeat =
                Math.floor((this.elaspedSinceLastUsed - this.castTime) /
                    Phaser.Timer.SECOND);

            if (repeat === this._repeatMax) {
                this._repeat = -1;
            }

            if (this._repeat === repeat) {
                this._repeat++;
                if (player.alive) {
                    player.target.damage(10);
                }
            }
        }
    }

    // tslint:disable-next-line:no-empty
    protected _effects(player: Character) {
    }
}

export class SkillPunchingArrow extends Skill {
    protected _fileIcon = "img/skills/archer_punchingarrow.png";
    protected _description = `L'archer tire une flèche perçante influgeant 
        70 points de dégats à son ennemi.`;

    constructor() {
        super("Flèche perçante", 200, 7 * Phaser.Timer.SECOND, 60);
        this._canTargetAlly = false;
        this._canTargetMyself = false;
        this._canTargetEnnemy = true;
    }

    protected _effects(player: Character) {
        player.target.damage(70);
    }
}

export class SkillResurection extends Skill {
    protected _fileIcon = "img/skills/wizard_resurrection.png";
    protected _description = `Le Sorcier fait appel aux Dieux 
        afin de redonner vie à un allié.`;

    constructor() {
        super("Résurection", Phaser.Timer.SECOND,
            20 * Phaser.Timer.SECOND, 100);
        this._canTargetMyself = false;
        this._canTargetAlly = true;
        this._canTargetEnnemy = false;
    }

    protected _effects(player: Character) {
        if (player.target.alive) {
            return;
        }
        player.target.alive = true;
        player.target.heal(0.5 * player.target.maxHealth);
        player.target.currentEnergy = 0;
        player.target.play("thrush_down");
    }
}

export class SkillRunePrison extends Skill {
    protected _fileIcon = "img/skills/wizard_runeprison.png";
    protected _description = `Le Sorcier emprisonne son ennemi dans une cage 
        runique, l'immobilisant pour une durée de 3s et lui infligeant 15 
        points de dégâts par seconde.`;

    private _repeat = -1;
    private _repeatMax = 3;

    constructor() {
        super("Prison runique",
            800, 6 * Phaser.Timer.SECOND, 70);
        this._canTargetAlly = false;
        this._canTargetMyself = false;
        this._canTargetEnnemy = true;
    }

    public apply(player: Character) {
        if (this._canUseSkill(player)) {
            this._repeat = 0;
        }

        super.apply(player);
    }

    public updateForElaspedTime(player: Character,
                                elaspedMS: number): any {
        super.updateForElaspedTime(player, elaspedMS);
        if (this._repeat !== -1 && this.elaspedSinceLastUsed > this.castTime) {
            const repeat =
                Math.floor((this.elaspedSinceLastUsed - this.castTime) /
                    Phaser.Timer.SECOND);

            if (repeat === this._repeatMax) {
                this._repeat = -1;
                player.target.unstun();
            }

            if (this._repeat === repeat) {
                this._repeat++;
                if (player.alive) {
                    player.target.stun();
                    player.target.damage(15);
                }
            }
        }
    }

    // tslint:disable-next-line:no-empty
    protected _effects(player: Character) {
    }
}

export class SkillSprint extends Skill {
    protected _fileIcon = "img/skills/warrior_sprint.png";
    protected _description = `Le Guerrier augmente de 20% sa vitesse de 
        déplacement.`;

    private _repeat = -1;
    private _repeatMax = 4;
    private _coef = 0.2;

    constructor() {
        super("Sprint", 0, 7 * Phaser.Timer.SECOND, 30);
        this._canTargetAlly = false;
        this._canTargetMyself = true;
        this._canTargetEnnemy = false;
    }

    public apply(player: Character) {
        if (this._canUseSkill(player, false)) {
            this._repeat = 0;
        }

        super.apply(player, false);
    }

    public updateForElaspedTime(player: Character,
                                elaspedMS: number): any {
        super.updateForElaspedTime(player, elaspedMS);
        if (this._repeat !== -1 && this.elaspedSinceLastUsed > this.castTime) {
            const repeat =
                Math.floor((this.elaspedSinceLastUsed - this.castTime) /
                    Phaser.Timer.SECOND);

            if (repeat === this._repeatMax) {
                this._repeat = -1;
                player.slow(this._coef);
            }

            if (this._repeat === repeat) {
                if (player.alive && this._repeat === 0) {
                    player.sprint(this._coef);
                }
                this._repeat++;
            }
        }
    }

    // tslint:disable-next-line:no-empty
    protected _effects(player: Character) {
    }
}

export class SkillUnbreakable extends Skill {
    protected _fileIcon = "img/skills/warrior_unbreakable.png";
    protected _description = `Le Guerrier se munit d'un bouclier pour une durée 
        de 4s lui permettant de survivre aux 30 prochains dommages qui lui 
        seront infligés.`;

    private _repeat = -1;
    private _repeatMax = 4;

    constructor() {
        super("Incassable", 200, 10 * Phaser.Timer.SECOND, 30);
        this._canTargetAlly = false;
        this._canTargetMyself = true;
        this._canTargetEnnemy = false;
    }

    public apply(player: Character) {
        if (this._canUseSkill(player, false)) {
            this._repeat = 0;
        }

        super.apply(player, false);
    }

    public updateForElaspedTime(player: Character,
                                elaspedMS: number): any {
        super.updateForElaspedTime(player, elaspedMS);

        if (this._repeat !== -1 && this.elaspedSinceLastUsed > this.castTime) {
            const repeat =
                Math.floor((this.elaspedSinceLastUsed - this.castTime) /
                    Phaser.Timer.SECOND);

            if (repeat === this._repeatMax) {
                this._repeat = -1;
                player.currentShield = 0;
            }

            if (this._repeat === repeat) {
                if (player.alive && this._repeat === 0) {
                    player.currentShield = 30;
                }
                this._repeat++;
            }
        }
    }

    // tslint:disable-next-line:no-empty
    protected _effects(player: Character) {
    }
}
