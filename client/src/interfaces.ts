export interface ICursor {
    down: boolean;
    left: boolean;
    right: boolean;
    up: boolean;
}
