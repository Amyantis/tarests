// tslint:disable:max-classes-per-file
import {
    IKeyEventFromServer,
    ITargetSelectionFromServer,
} from "../../protocol/sharedInterfaces";
import {Game} from "./Game";
import {phaserKeyToSkillIndex} from "./keys";

export abstract class PlayerCommand {
    protected _game: Game;

    constructor(game: Game) {
        this._game = game;
    }

    public abstract handleCommand(): void;
}

export class KeyCommand extends PlayerCommand {
    private _keyEvent: IKeyEventFromServer;

    constructor(game: Game, keyEvent: IKeyEventFromServer) {
        super(game);
        this._keyEvent = keyEvent;
    }

    public handleCommand() {
        const character =
            this._game.characterList.get(this._keyEvent.clientId);

        if (this._keyEvent.key.isDown &&
            phaserKeyToSkillIndex.has(this._keyEvent.key.keyId)) {
            const skillIndex =
                phaserKeyToSkillIndex.get(this._keyEvent.key.keyId) - 1;
            character.useSkill(skillIndex);
        }

        switch (this._keyEvent.key.keyId) {
            case Phaser.Keyboard.UP:
            case Phaser.Keyboard.Z:
                character.cursor.up = this._keyEvent.key.isDown;
                break;
            case Phaser.Keyboard.S:
            case Phaser.Keyboard.DOWN:
                character.cursor.down = this._keyEvent.key.isDown;
                break;
            case Phaser.Keyboard.Q:
            case Phaser.Keyboard.LEFT:
                character.cursor.left = this._keyEvent.key.isDown;
                break;
            case Phaser.Keyboard.D:
            case Phaser.Keyboard.RIGHT:
                character.cursor.right = this._keyEvent.key.isDown;
                break;
            case Phaser.Keyboard.E:
                if (!this._keyEvent.key.isDown) {
                    if (this._game.goldenTrident.player === character) {
                        this._game.goldenTrident.player = null;
                    } else {
                        this._game.goldenTrident.player = character;
                    }
                }
            default:
                ;
        }
    }
}

export class TargetSelectionCommand extends PlayerCommand {
    private _targetSelection: ITargetSelectionFromServer;

    constructor(game: Game,
                targetSelection: ITargetSelectionFromServer) {
        super(game);
        this._targetSelection = targetSelection;
    }

    public handleCommand() {
        const character =
            this._game.characterList.get(this._targetSelection.clientId);
        const target =
            this._game.characterList.get(this._targetSelection.targetId);
        character.target = target;
    }
}
