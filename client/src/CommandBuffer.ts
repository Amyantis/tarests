import {PlayerCommand} from "./Commands";
export class CommandBuffer {
    private _commandsNotAssignedToARound = new Array<PlayerCommand>();
    private _commandsForRounds = new Array<PlayerCommand[]>();

    public storeNewCommand(playerCommand: PlayerCommand) {
        this._commandsNotAssignedToARound.push(playerCommand);
    }

    public makeARound(numberCommandForThisRound: number) {
        if (this._commandsNotAssignedToARound.length <
            numberCommandForThisRound) {
            console.error("Unhandled request to make a round.");
        }
        // TODO: can we simplify following loop?
        const commandsForThisRound = new Array<PlayerCommand>();
        for (let i = 0; i < numberCommandForThisRound; i++) {
            const playerCommand = this._commandsNotAssignedToARound.shift();
            commandsForThisRound.push(playerCommand);
        }
        this._commandsForRounds.push(commandsForThisRound);
    }

    public hasARoundToProcess() {
        return this._commandsForRounds.length > 0;
    }

    public getCommandsForNextRound() {
        return this._commandsForRounds.shift();
    }
}
