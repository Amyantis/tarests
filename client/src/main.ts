import {
    IClientInfo,
    IGameSettings,
    IKeyEventFromServer, IStats,
    ITargetSelectionFromServer,
    Sex,
} from "../../protocol/sharedInterfaces";
import {CommandBuffer} from "./CommandBuffer";
import {KeyCommand, TargetSelectionCommand} from "./Commands";
import {Game} from "./Game";

window.onload = () => {
    $(".starting_screen tr:not(:first)").hide();
    $(".starting_screen").hide();
    $(".game_screen").hide();
    $(".ending_screen").hide();

    const skipIntroductionScreen = () => {
        $(document).unbind("keypress");
        $(".introduction_screen").hide();
        $(".starting_screen").show();
    };

    // TODO: use this only during development
    // skipIntroductionScreen();

    $(document).keypress((e) => {
        if (e.which === 13) {
            // press enter
            skipIntroductionScreen();
            $(document).keypress(null);
        }
    });

    $(".introduction_screen button").click(skipIntroductionScreen);

    const socket = io.connect("http://localhost:8080");

    const commandBuffer = new CommandBuffer();
    const game = new Game(socket, commandBuffer, false);

    $(window).resize(() => {
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.scale.setGameSize(
            $(window).width(),
            $(window).height());
        // TODO: resize world too? (to fill black screen)
    });

    // socket.on("updateClientsInfos", game.updateClientsInfos.bind(game));
    socket.on("updateClientsInfos", (clientsInfos: IClientInfo[]) => {
        const startingScreenRows = $(".starting_screen tr:not(:first)");
        startingScreenRows.hide();
        startingScreenRows.removeClass("success");

        let i = 1;
        for (const clientInfos of clientsInfos) {
            const trAssociated = $(`.starting_screen tr:eq(${i})`);
            trAssociated.find("input").prop("disabled", true);
            trAssociated.find("select").prop("disabled", true);
            trAssociated.find("option").prop("selected", false);

            const clientAlias = trAssociated.find(`input[name="alias"]`);
            const imgEditAlias = trAssociated.find(`img[name="edit_alias"]`);
            const selectStatus = trAssociated.find(`select[name="status"]`);
            const selectRace = trAssociated.find(`select[name="race"]`);
            const selectClass = trAssociated.find(`select[name="class"]`);
            const selectSex = trAssociated.find(`select[name="sex"]`);

            selectStatus.unbind();
            selectRace.unbind();
            selectClass.unbind();
            selectSex.unbind();

            imgEditAlias.css("visibility", "hidden");

            if (clientInfos.clientId === socket.id) {
                trAssociated.addClass("success");
                clientAlias.prop("disabled", false);
                imgEditAlias.css("visibility", "visible");
                trAssociated.find("select").prop("disabled", false);
                selectStatus.change(() => {
                    if (selectStatus.find(`option:eq(0)`).is(":selected")) {
                        socket.emit("setState", true);
                    } else {
                        socket.emit("setState", false);
                    }
                });
                clientAlias.change(() => {
                    if (clientAlias.val().length >= 8) {
                        socket.emit("setName", clientAlias.val());
                    }
                });
                selectRace.change(() => {
                    const selectedRaceId: number =
                        selectRace.find("option:selected").index();
                    socket.emit("setTeamId", selectedRaceId);
                });
                selectClass.change(() => {
                    const selectedClassId: number =
                        selectClass.find("option:selected").index();
                    socket.emit("setClassId", selectedClassId);
                });
                selectSex.change(() => {
                    const selectedSex: Sex =
                        selectSex.find("option:selected").index() === 0 ?
                            Sex.Male : Sex.Female;
                    console.log("selectedSex", selectedSex);
                    socket.emit("setSex", selectedSex);
                });
            }

            // display player attributes
            clientAlias.val(clientInfos.clientAlias);
            const raceToSelect =
                selectRace.find(`option:eq(${clientInfos.teamId})`);
            raceToSelect.prop("selected", true);
            const classToSelect =
                selectClass.find(`option:eq(${clientInfos.classId})`);
            classToSelect.prop("selected", true);
            const idSexToSelect = clientInfos.sex === Sex.Male ? 0 : 1;
            const sexToSelect =
                selectSex.find(`option:eq(${idSexToSelect})`);
            sexToSelect.prop("selected", true);
            const statusToSelect =
                selectStatus.find(`option:eq(${clientInfos.isReady ? 0 : 1})`);
            statusToSelect.prop("selected", true);

            trAssociated.show();
            i++;
        }
    });

    socket.on("startGame", (gameSettings: IGameSettings) => {
        $(".introduction_screen").hide();
        $(".starting_screen").hide();
        $(".game_screen").show();
        game.startGame(gameSettings);
    });

    socket.on("makeARound", (numberCommandForThisRound: number) => {
        commandBuffer.makeARound(numberCommandForThisRound);
        if (game.remainingTimeForUpdatingMS === 0) {
            game.startNextRound(commandBuffer.getCommandsForNextRound());
        }
    });

    socket.on("storePlayerKeyEvent", (keyEvent: IKeyEventFromServer) => {
        console.log("keyEvent", keyEvent);
        const keyCommand =
            new KeyCommand(game, keyEvent);
        commandBuffer.storeNewCommand(keyCommand);
    });

    socket.on("storePlayerTargetSelectionEvent",
        (targetSelectionEvent: ITargetSelectionFromServer) => {
            const targetSelectionCommand =
                new TargetSelectionCommand(
                    game,
                    targetSelectionEvent);
            commandBuffer.storeNewCommand(targetSelectionCommand);
        });

    socket.on("setSpeedRatio", (speedRatio: number) => {
        const oldSpeedRatio = game.speedRatio;
        setTimeout(() => {
            game.speedRatio = oldSpeedRatio;
        }, game.tickRate);
        game.speedRatio = speedRatio;
    });
    socket.on("latency", () => {
        socket.emit("latency");
    });
    socket.on("latencyStats", (latency: IStats) => {
        console.log("Latency:", latency);
    });
};
