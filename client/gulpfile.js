const gulp = require('gulp');
const gulpLivereload = require('gulp-livereload');
const gulpPlumber = require('gulp-plumber');
const gulpWebpack = require('gulp-webpack');

gulp.task('client_copy_libs', () => {
    const libs = [
        'node_modules/jquery/dist/jquery.js',
        'node_modules/phaser-ce/build/phaser.js',
        'node_modules/socket.io-client/dist/socket.io.js',
    ];
    return gulp
        .src(libs)
        .pipe(gulp.dest('./public/js'));
});

gulp.task('client_build', () => {
    return gulp.src('./src/index.ts')
        .pipe(gulpPlumber())
        .pipe(gulpWebpack(require('./webpack.config.js')))
        .pipe(gulp.dest('./public/js'))
        .pipe(gulpLivereload());
});

gulp.task('client_watch', ['default'], function() {
    gulpLivereload.listen();
    gulp.watch('./src/**/*', ['client_build']);
});

gulp.task('default', ['client_copy_libs', 'client_build']);
