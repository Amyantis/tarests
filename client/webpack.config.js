module.exports = {
    devtool: 'inline-source-map',
    entry: './src/main.ts',
    externals: {
        'phaser': 'Phaser',
        'socket.io-client': 'io',
    },
    module: {
        loaders: [
            {test: /\.tsx?$/, loader: 'ts-loader', exclude: /node_modules/},
        ],
    },
    output: {
        filename: 'main.js',
    },
    resolve: {
        extensions: ['', '.tsx', '.ts', '.js'],
    },
};
