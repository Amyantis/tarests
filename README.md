# Tares

A MOBA like game.

A typescript / node_js / html5 game by Théophile DANCOISNE.

## Development

### File Structure
- client
- server
- documentation: plantuml state diagram explaining network management (use npm task `puml`).
- protocol: typescript interfaces for communication between client and server.

### Code checking
```
npm install
```

Check typescript / node_js  code
```
npm run tslint --fix
npm run eslint --fix
```

### Client
```
cd client
npm install
```

Build (and copy libs):
```
gulp
```
nb: use `npm run gulp` instead to use `gulp` from modules.

Watch:
```
gulp client_watch
```

Serve client files:
```
node index.js
```

### Server
```
cd server
npm install
```

Build:
```
gulp
```

Watch:
```
gulp server_watch
```

Run:
```
node dist/main.js
```

### Before pushing
```
rm client/public/js/*
rm server/dist/*
```
Re-build and test.

## License

MIT © 2016 Théophile DANCOISNE <dancoisne.theophile@gmail.com>


## Credit
http://gaurav.munjal.us/Universal-LPC-Spritesheet-Character-Generator/