export enum Sex {
    Female,
    Male,
}

export interface IPosition {
    x: number;
    y: number;
}

export interface IClientInfo {
    classId: number;
    clientAlias: string;
    clientId: string;
    isReady: boolean;
    sex: Sex;
    teamId: number;
}

export interface IPlayerInfo {
    clientInfo: IClientInfo;
    initialPosition: IPosition;
}

export interface IKeyEventFromClient {
    keyId: number;
    isDown: boolean;
}

export interface IKeyEventFromServer {
    clientId: string;
    key: IKeyEventFromClient;
}

export interface ITargetSelectionFromServer {
    clientId: string;
    targetId: string;
}

export interface IGameSettings {
    initialPlayerInfos: IPlayerInfo[];
    controlledclientId: string;
    tickRate: number;
}

export interface IStats {
    mean: number;
    median: number;
    variance: number;
}
