// TODO: find a way to enable no-var-requires
// require statement not part of an import statement
/* tslint:disable */
const express = require("express");
import * as http from "http";
const socketIo = require("socket.io");
/* tslint:enable */

// TODO: we should remove serving socket.io file for client here

export abstract class Server {
    public static readonly PORT = 8080;
    protected _io: SocketIO.Server;
    private _app: any;
    private _server: http.Server;
    private _port: number;

    constructor() {
        this._app = express();
        this._server = http.createServer(this._app);
        this._port = process.env.PORT || Server.PORT;
        this._io = socketIo(this._server);
        this.listen();
    }

    get app(): any {
        return this._app;
    }

    protected listen() {
        this._server.listen(this._port, () => {
            console.log("Running server on port %s", this._port);
        });
    }
}
