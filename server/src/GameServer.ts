import {
    IClientInfo,
    IGameSettings,
    IKeyEventFromClient,
    IKeyEventFromServer,
    IPlayerInfo,
    IStats,
    ITargetSelectionFromServer,
    Sex,
} from "../../protocol/sharedInterfaces";
import {spawnPosition} from "./constants";
import {Server} from "./GenericServer";
import {IClient} from "./interfaces";

/* tslint:disable */
const d3Array = require("d3-array");
/* tslint:enable */

export class GameServer extends Server {
    private _numberOfTeam = 4;
    private _clients = new Map<string, IClient>();
    private _gameHasStarted = false;
    private _nbCommandsForThisRound = 0;
    private _numberOfRoundDelayed = 1;
    private _tickRate = 80;
    private _clock = 0;
    private _mostDelayedClientClock = -1;

    protected listen(): any {
        super.listen();

        this._io.on("connection", (socket: SocketIO.Socket) => {
            if (this._gameHasStarted) {
                return;
            }

            const clientId = socket.id;
            const clientAlias = clientId;
            console.log("New Client", clientId);

            const teamId = this._clients.size % this._numberOfTeam;

            // TODO: adapt this to the number of existing classes
            const numberOfClassesAvailable = 3;
            const classId =
                Math.round(Math.random()) * (numberOfClassesAvailable - 1);

            const sex =
                Math.round(Math.random()) === 0 ? Sex.Male : Sex.Female;

            const clientInfo: IClientInfo = {
                classId,
                clientId,
                clientAlias,
                isReady: false,
                sex,
                teamId,
            };

            const newClient: IClient = {
                clientInfo,
                socket,
                lastLatencyDate: -1,
                lastProcessedRound: -1,
                latencies: new Array<number>(),
            };

            this._clients.set(clientId, newClient);

            this._broadcastBasicClientInfos();

            socket.on("disconnect", () => {
                console.log("Client disconnected", clientId);
                this._clients.delete(clientId);

                // TODO: inform players

                if (this._clients.size < 1) {
                    this._nbCommandsForThisRound = 0;
                    this._gameHasStarted = false;
                    this._clock = 0;
                    this._mostDelayedClientClock = -1;
                }
            });
            socket.on("latency", () => {
                if (newClient.lastLatencyDate === -1) {
                    return;
                }
                const ping = Date.now() - newClient.lastLatencyDate;
                newClient.latencies.push(ping);

                if (newClient.latencies.length > 10) {
                    const statsOnClientLatencies =
                        this.computeStatsOnClientLatencies(newClient);
                    if (statsOnClientLatencies.mean > 90) {
                        console.log(
                            clientId,
                            statsOnClientLatencies.mean,
                            statsOnClientLatencies.variance);
                    }
                    socket.emit("latencyStats", statsOnClientLatencies);
                }
            });
            setInterval(() => {
                newClient.lastLatencyDate = Date.now();
                socket.emit("latency");
            }, 10 * 1e3);
            socket.on("setName", (newName: string) => {
                if (!this._isAliasAlreadyUsed(newName) && newName.length >= 8) {
                    clientInfo.clientAlias = newName;
                }
                this._broadcastBasicClientInfos();
            });
            socket.on("setState", (newState: boolean) => {
                clientInfo.isReady = newState;
                this._broadcastBasicClientInfos();
                this._handleClientGetReady();
            });
            socket.on("setTeamId", (newTeamId: number) => {
                clientInfo.teamId = newTeamId;
                this._broadcastBasicClientInfos();
            });
            socket.on("setClassId", (newClassId: number) => {
                clientInfo.classId = newClassId;
                this._broadcastBasicClientInfos();
            });
            socket.on("setSex", (newSex: Sex) => {
                console.log("newSex", newSex);
                clientInfo.sex = newSex;
                this._broadcastBasicClientInfos();
            });
            socket.on("setRoundAsProcessed", () => {
                this._clients.get(clientId).lastProcessedRound++;
                if (this._readyToMakeARound()) {
                    for (const client of this._clients.values()) {
                        const isDelayed =
                            client.lastProcessedRound +
                            this._numberOfRoundDelayed <
                            this._clock;
                        if (isDelayed) {
                            const roundDelayed =
                                this._clock -
                                client.lastProcessedRound -
                                this._numberOfRoundDelayed;
                            console.warn(
                                "Client", client.clientInfo.clientId,
                                "is delayed by", roundDelayed, "round(s).");
                            let speedRatio = Math.min(roundDelayed, 3);
                            socket.emit("setSpeedRatio", speedRatio);
                        }
                    }

                    this._makeARound();
                }

                const clients = Array.from(this._clients.values());
                const lastProcessedRounds =
                    clients.map((client) => client.lastProcessedRound);
                this._clock = Math.max(...lastProcessedRounds);
            });
            socket.on("broadcastKeyEvent", (keyEvent: IKeyEventFromClient) => {
                const keyEventFromServer: IKeyEventFromServer = {
                    clientId,
                    key: keyEvent,
                };
                this._io.emit("storePlayerKeyEvent", keyEventFromServer);
                this._nbCommandsForThisRound++;
            });
            socket.on("broadcastTargetSelectionEvent", (targetId: string) => {
                const targetSelectionFromServer: ITargetSelectionFromServer = {
                    clientId,
                    targetId,
                };
                this._io.emit(
                    "storePlayerTargetSelectionEvent",
                    targetSelectionFromServer);
                this._nbCommandsForThisRound++;
            });
        });
    }

    private _isAliasAlreadyUsed(alias: string) {
        for (const client of this._clients.values()) {
            if (client.clientInfo.clientAlias === alias) {
                return true;
            }
        }
        return false;
    }

    private computeStatsOnClientLatencies(client: IClient): IStats {
        let mean = d3Array.mean(client.latencies);
        mean = Math.round(mean * 100) / 100;
        let variance = d3Array.variance(client.latencies);
        variance = Math.round(variance * 100) / 100;
        let median = d3Array.median(client.latencies);
        median = Math.round(median * 100) / 100;
        client.latencies = new Array<number>();
        return {mean, median, variance};
    }

    private _makeARound() {
        const numberCommandForThisRound = this._nbCommandsForThisRound;
        this._nbCommandsForThisRound = 0;
        for (const client of this._clients.values()) {
            client.clientInfo.isReady = false;
        }
        this._io.emit("makeARound", numberCommandForThisRound);
    }

    private _readyToMakeARound() {
        for (const client of this._clients.values()) {
            if (this._clock - client.lastProcessedRound > 10) {
                return false;
            }
            if (client.lastProcessedRound === this._mostDelayedClientClock) {
                return false;
            }
        }
        this._mostDelayedClientClock++;
        return true;
    }

    private _areClientReady() {
        for (const client of this._clients.values()) {
            if (!client.clientInfo.isReady) {
                return false;
            }
        }
        return true;
    }

    private _broadcastBasicClientInfos() {
        const tmpClients = new Array<IClientInfo>();
        for (const client of this._clients.values()) {
            tmpClients.push({
                classId: client.clientInfo.classId,
                clientAlias: client.clientInfo.clientAlias,
                clientId: client.clientInfo.clientId,
                isReady: client.clientInfo.isReady,
                sex: client.clientInfo.sex,
                teamId: client.clientInfo.teamId,
            });
        }
        this._io.emit("updateClientsInfos", tmpClients);
    }

    private _startGame() {
        console.warn("Starting game.");

        const initialPlayerInfos = new Array<IPlayerInfo>();
        for (const client of this._clients.values()) {
            console.log(client.clientInfo);
            initialPlayerInfos.push({
                clientInfo: client.clientInfo,
                initialPosition: spawnPosition[client.clientInfo.teamId],
            });
        }

        for (const client of this._clients.values()) {
            const gameSettings: IGameSettings = {
                initialPlayerInfos,
                controlledclientId: client.clientInfo.clientId,
                tickRate: this._tickRate,
            };
            client.socket.emit("startGame", gameSettings);
        }

        for (let i = 0; i < this._numberOfRoundDelayed; i++) {
            this._makeARound();
        }

        this._gameHasStarted = true;
    }

    private _handleClientGetReady() {
        if (!this._gameHasStarted && this._areClientReady()) {
            this._startGame();
        }
    }
}
