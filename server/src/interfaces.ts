import {IClientInfo} from "../../protocol/sharedInterfaces";

export interface IClient {
    clientInfo: IClientInfo;
    socket: SocketIO.Socket;
    lastProcessedRound: number;
    lastLatencyDate: number;
    latencies: number[];
}
