const gulp = require('gulp');
const gulpBabel = require('gulp-babel');
const gulpTypescript = require('gulp-typescript');
const es2015Node5 = require('babel-preset-es2015-node5');

const tsProject = gulpTypescript.createProject('tsconfig.json');

gulp.task('server_build', function() {
    const tsFilesCompiled =
        tsProject.src()
        .pipe(tsProject());

    return tsFilesCompiled
        .pipe(gulpBabel({
            presets: [es2015Node5],
        }))
        .pipe(gulp.dest('./dist'));
});

gulp.task('server_watch', ['server_build'], () => {
    gulp.watch('./src/*.ts', ['server_build']);
});

gulp.task('default', ['server_build']);
